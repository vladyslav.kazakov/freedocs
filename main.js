require.config({
    paths: {
        lodash: './vendor/lodash',
        handlebars: './vendor/handlebars.min',
        handlebarsExtended: './utils/handlebars_helper',
        bootstrap: './vendor/bootstrap.min',
        diffMatchPatch: './vendor/diff_match_patch.min',
        jquery: './vendor/jquery.min',
        locales: './locales/locale',
        pathToRegexp: './vendor/path-to-regexp/index',
        prettify: './vendor/prettify/prettify',
        semver: './vendor/semver.min',
        utilsSampleRequest: './utils/send_sample_request',
        webfontloader: './vendor/webfontloader'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        },
        diffMatchPatch: {
            exports: 'diff_match_patch'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        handlebarsExtended: {
            deps: ['jquery', 'handlebars'],
            exports: 'Handlebars'
        },
        prettify: {
            exports: 'prettyPrint'
        }
    },
    urlArgs: 'v=' + (new Date()).getTime(),
    waitSeconds: 15
});

require([
    './data.js',
    './core/utils.js',
    './core/class.js',
    './core/documentationClasses.js',
    './core/baseClasses.js',
    // './core/documentationView.js',
    './core/swagger.js',
    'lodash',
    'jquery',
    'locales',
    'handlebarsExtended',
    'prettify',
    'utilsSampleRequest',
    'semver',
    'webfontloader',
    'bootstrap',
    'pathToRegexp'
],

function(data, utils, Class, DocumentationClasses, BaseClasses, swagger, _, $, locale, Handlebars, prettyPrint, sampleRequest, semver, WebFont) {
    'use strict';

//REGION CONTROLLER 
    const DocumentationController = Class.create('DocumentationController', null, {
        initialize: function(documentation) {
            const self = this;
            defineProperties();
            setTitle();
            setProjectContent();
            self.version = _.first(self._versions);
            window.addEventListener('hashchange',
                function(e) {
                    const oldID = e.oldURL.substring(e.oldURL.lastIndexOf('#'));
                    const newID = e.newURL.substring(e.newURL.lastIndexOf('#'));
                    self._setArticleVisibility(oldID, false);
                    self._setArticleVisibility(newID, true);
                },
                false
            );
            $('#loader').remove();

            function setProjectContent() {
                if ( self._versions.length < 2 )
                    return;

                const templateProject = Handlebars.compile($('#template-project').html());
                const projectContent = templateProject(self._versions);
                $('#project').html(projectContent);

                Object.defineProperty(self, '_htmlVersion', {value: $('#version strong')});
                $('#versions li a').on('click', function(e) {
                    e.preventDefault();
                    self.version = $(this).html();
                });

                Object.defineProperty(self, '_htmlVersionToCompare', {value: $('#version-to-compare strong')});
                Object.defineProperty(self, '_htmlVersionsToCompare', {value: $('#versions-to-compare li')});
                self._htmlVersionsToCompare.children('a').on('click', function(e) {
                    e.preventDefault();
                    self.versionToCompare = $(this).html();
                });

                $('#compare').on('click', function(e) {
                    e.preventDefault();
                    self._setView(self.version, self.versionToCompare);
                });
            }

            function setTitle() {
                if ( !_.isNull(self._documentation.title) )
                    $(document).attr('title', self._documentation.title);
                else
                    $(document).attr('title', '');
            }

            function defineProperties() {
                Object.defineProperty(self, '_documentation', {value: documentation});
                Object.defineProperty(self, '_templateArticles', {value: Handlebars.compile($('#template-articles').html())});
                Object.defineProperty(self, '_templateSidenav',  {value: Handlebars.compile($('#template-sidenav').html())});
                Object.defineProperty(self, '_versions', {value: documentation.versions.getKeys().reverse()});
                Object.defineProperty(self, '_views', {value: Object.create(null)});
                self.definePrimitiveProperty('version', 'string', null, self._setVersion);
                self.definePrimitiveProperty('versionToCompare', 'string', null, self._setVersionToCompare);
                Object.defineProperty(self, '_htmlSidenav', {value: $('#sidenav')});
                Object.defineProperty(self, '_htmlArticles', {value: $('#articles')});
            }
        },

        _createView: function(version, versionToCompare) {
            const self = this;
            const docVersion = this._documentation.versions[version];
            const docVersionToCompare = this._documentation.versions[versionToCompare];
            const diff = docVersion.diff(docVersionToCompare);
            const newView = {
                navigationContent: this._templateSidenav(diff.navigation),
                articlesContent: this._templateArticles(diff.articles),
                defaultArticle: ''
            };
            if ( "default" in diff.articles )
                newView.defaultArticle = '#default';
            return newView;
        },

        _setView:  function(version, versionToCompare) {
            if ( !(version in this._documentation.versions) )
                throw new Error(`undefined version [${version}]`);

            if ( !(versionToCompare in this._documentation.versions) )
                throw new Error(`undefined version [${versionToCompare}]`);

            let views = this._views[version];
            if ( !utils.assigned(views) )
                views = ( this._views[version] = Object.create(null) );
            let view = views[versionToCompare];
            if ( !utils.assigned(view) )
                view = ( views[versionToCompare] = this._createView(version, versionToCompare) );
            this._htmlSidenav.html(view.navigationContent);
            this._htmlArticles.html(view.articlesContent);
            this._view = view;
            this._setArticleVisibility(window.location.hash, true);
        },

        _setVersion: function(version) {
            if ( _.isNull(version) )
                return;

            if ( utils.assigned(this._htmlVersion) ) {
                this._htmlVersion.html(version);
                const versionIdx = this._versions.indexOf(version);
                const versionToCompareIdx = this._versions.indexOf(this.versionToCompare);
                if ( versionToCompareIdx < versionIdx )
                    this.versionToCompare = version;
                let li = this._htmlVersionsToCompare.first();
                for (let i = 0; i < versionIdx; ++i, li = li.next())
                    li.addClass('hide');
                for (let i = versionIdx; i <= this._versions.length - 1; ++i, li = li.next())
                    li.removeClass('hide');
            }

            this._setView(version, version);
        },

        _setVersionToCompare: function(version) {
            if ( _.isNull(version) )
                return;
            if ( !(version in this._documentation.versions) )
                throw new Error(`undefined version [${version}]`);
            this._htmlVersionToCompare.html(version);
        },

        _setArticleVisibility: function(id, isVisible) {
            id = id.replace(/\//g, '\\\/').replace(/{/g, '\\{').replace(/}/g, '\\}');
            if ( isVisible && (id !== this._view.defaultArticle) && ($(id).length === 0) ) {
                window.location.hash = this._view.defaultArticle;
                return;
            }
            const a = this._htmlSidenav.children('>li>a[href="' + id + '"]');
            const li = a.parent();
            if ( isVisible ) {
                $(id).removeClass('hide');
                a.focus();
                li.addClass('active');
            } else {
                $(id).addClass('hide');
                li.removeClass('active');
            }
        }
    }).register();
//ENDREGION CONTROLLER   

//REGION 'other'
    function loadGoogleFontCss() {
        const host = document.location.hostname.toLowerCase();
        const protocol = document.location.protocol.toLowerCase();
        let googleCss = '//fonts.googleapis.com/css?family=Source+Code+Pro|Source+Sans+Pro:400,600,700';
        if ( (host === 'localhost') || !host.length || (protocol === 'file:') )
            googleCss = 'http:' + googleCss;
        $('<link/>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: googleCss
        }).appendTo('head');
    }
//ENDREGION 'other'

//REGION 'main'
    loadGoogleFontCss();
    DocumentationClasses.addHandler('swagger', swagger); 
    const documentation = new DocumentationClasses.Documentation();
    documentation.deserialize(data);
    const documentationController = new DocumentationController(documentation);
    let total = 0;
    _.forEach(DocumentationClasses.statistics, function(value, key) {
        if ( value.length !== 0 )
            console.log(`${key}: ${value.length}`);
        total += value.length;
    });
    console.log(`total: ${total}`);
//ENDREGION 'main'

});
