define(['../vendor/lodash', './utils.js', './errors.js', './class.js'], function(_, utils, errors, Class) {
    'use strict';

    const exports = {};

    function addToExports(name, value) {
        Object.defineProperty(exports, name, {
            value: value
        });
    }

    const STATE_NEW = 'new';
    const STATE_NOT_CHANGED = 'notChanged';
    const STATE_CHANGED = 'changed';
    const STATE_DELETED = 'deleted';

    const AbstractObject = Class.create('AbstractObject', null, {
        initialize: function() {
            const self = this;
            Object.defineProperty(self, 'type', {
                configurable: false,
                enumerable: true,
                get: function() {
                    return self.constructor.name;
                },
                set: function(value) {
                    if ( value !== self.type )
                        throw new errors.CustomError(self, 'typeSetter', `attempt to change type, type [${self.type}], newType [${value}]`);
                }
            });
        },

        deserialize: function(object, skip = {}) {
            try {
                if ( !_.isObjectLike(object) )
                    throw new IncorrectParameter('object');
                if ( !_.isObjectLike(skip) )
                    throw new IncorrectParameter('skip');
                this.doDeserialize(object, skip);
                return this;
            } catch(e) {
                errors.raiseCatchError(this, 'deserialize', e);
            }
        },

        diff: function(object) {
            try {
                if ( !_.isObjectLike(object) )
                    throw new IncorrectParameter('object');
                if ( object.constructor.prototype !== this.constructor.prototype )
                    errors.raiseError(`incompatible types, self [${utils.getType(this)}], object [${utils.getType(object)}]`);
                return this.doDiff(object);
            } catch(e) {
                errors.raiseCatchError(this, 'diff', e);
            }
        },

        doDeserialize: function(object, skip) {
            errors.raiseError(this, 'doDeserialize', 'unsupported');
        },

        doDiff: function(object) {
            throw new errors.CustomError(this, 'doDiff', 'unsupported');
        },

        // clone: function() {
            // const result = {};
            // _.forEach(this, function(value, key) {
                // if ( _.isObjectLike(value) )
                    // value = _.clone(value);
                // result[key] = value;
            // });
            // return result;
        // }
    }).register();

    const CustomObject = Class.create('CustomObject', AbstractObject, {
        doDeserialize: function(object, skip = {}) {
            const self = this;
            _.forEach(object, function(value, key) {
                try {
                    const descriptor = Object.getOwnPropertyDescriptor(self, key);
                    if ( !utils.assigned(descriptor) || !descriptor.enumerable )
                        throw new Error('undefined or non-enumerable');
                    if ( key in skip )
                        return;
                    const selfValue = self[key];
                    if ( _.isObjectLike(selfValue) && utils.assigned(selfValue.deserialize) ) {
                        selfValue.deserialize(value);
                        return;
                    }
                    self[key] = _.clone(value);
                } catch(e) {
                    errors.raiseCatchError('CustomObject', 'doDeserialize', e, `key [${key}]`);
                }
            });
        },

        doDiff: function(object) {
            if ( this === object )
                return _.clone(this);
            const result = {};
            result.state = STATE_NOT_CHANGED;
            _.forEach(this, function(value, key) {
                const comparableValue = object[key];
                if ( _.isObjectLike(value) && utils.assigned(value.diff) ) {
                    value = value.diff(comparableValue);
                    if ( value.state === STATE_CHANGED )
                        result.state = STATE_CHANGED;
                } else if ( value !== comparableValue ) {
                    result[`${key}_diff`] = comparableValue;
                    result.state = STATE_CHANGED;
                } else
                    value = _.clone(value);
                result[key] = value;
            });
            return result;
        }
    }).register();

    const CustomDictionary = Class.create('CustomDictionary', AbstractObject, {
        initialize: function(itemType = CustomObject) {
            if ( !_.isFunction(itemType) || !itemType.isClassInheritsFrom(CustomObject) )
                throw new errors.CustomError('CustomDictionary', 'initialize', `incorrect itemType [${utils.getType(itemType)}]`);
            utils.defineMethod(this, 'itemType', itemType);
        },

        doDeserialize: function(object) {
            const self = this;
            _.forEach(object, function(newItem, index) {
                try {
                    let currItem = self[index];
                    if ( utils.assigned(currItem) && utils.assigned(newItem.type) && (currItem.type !== newItem.type) ) {
                        delete self[index];
                        currItem = undefined;
                    }
                    if ( !utils.assigned(currItem) ) {
                        let itemType;
                        //debugger;
                        if ( utils.assigned(newItem.type) )
                            itemType = Class.getClassByName(newItem.type);
                        currItem = self.add(index, itemType);
                    }
                    currItem.deserialize(newItem);
                } catch(e) {
                    errors.raiseCatchError('CustomDictionary', 'doDeserialize', e, `index [${index}]`);
                }
            });
        },

        doDiff: function(object) {
            const self = this;
            const comparableObject = {};
            _.forEach(object, function(value, index) {
                comparableObject[index] = value;
            });
            let state = STATE_NOT_CHANGED;
            const resultObject = {};
            _.forEach(self, function(value, index) {
                if ( index in comparableObject ) {
                    value = value.diff(comparableObject[index]);
                    if ( value.state !== STATE_NOT_CHANGED )
                        state = STATE_CHANGED;
                    resultObject[index] = value;
                    delete comparableObject[index];
                } else {
                    const item = _.clone(value);
                    item.state = STATE_NEW;
                    resultObject[index] = item;
                    state = STATE_CHANGED;
                }
            });

            _.forEach(comparableObject, function(value, index) {
                value = _.clone(value);
                value.state = STATE_DELETED;
                resultObject[index] = value;
                state = STATE_CHANGED;
            });

            Object.defineProperty(resultObject, 'state', {});
            return resultObject;
        },

        add: function(index, itemType) {
            const self = this;
            if ( !utils.assigned(itemType) )
                itemType = self.itemType;
            try {
                if ( utils.assigned(self[index]) )
                    throw new Error('already exists');
                /*jshint newcap: false */
                const newItem = new itemType();
                if ( !(newItem instanceof self.itemType) )
                    throw new Error('incorrect itemType');
                Object.defineProperty(this, index, {
                    configurable: true,
                    enumerable: true,
                    writable: false,
                    value: newItem
                });
                return newItem;
            } catch(e) {
                errors.raiseCatchError('CustomDictionary', 'doDeserialize', e,
                    `index [${index}], self.itemType [${utils.getType(self.itemType)}], itemType [${utils.getType(itemType)}]`
                );
            }
        },

        getOrAdd: function(index, itemType) {
            try {
                let currItem = this[index];
                if ( utils.assigned(currItem) && utils.assigned(itemType) )
                    if ( (currItem.constructor.prototype !== itemType) && !(currItem.constructor.prototype instanceof itemType) )
                        throw new Error('incompatible types');

                if ( !utils.assigned(currItem) )
                    currItem = this.add(index, itemType);
                return currItem;
            } catch(e) {
                errors.raiseCatchError('CustomDictionary', 'getOrAdd', e,
                    `index [${index}], self.itemType [${utils.getType(this.itemType)}], itemType [${utils.getType(itemType)}]`
                );
            }
        },

        getKeys: function(self) {
            if ( !utils.assigned(self) )
                self = this;
            return this.doGetKeys(self);
        },

        doGetKeys: function(self) {
            return Object.keys(self);
        }
    }).register();

    const Item = Class.create('Item', CustomObject, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('order', 'number', 5);
        }
    }).register();
    const Items = Class.create('Items', CustomDictionary, {
        initialize: function($super, itemType = Item) {
            if ( !_.isFunction(itemType) || !itemType.isClassInheritsFrom(CustomObject) || ( (itemType !== Item) && !(itemType.prototype instanceof Item) ) )
                throw new errors.CustomError('Items', 'initialize', `incorrect itemType [${utils.getType(itemType)}]`);
            $super(itemType);
        },

        doGetKeys: function(self) {
            const keys = Object.keys(self).sort(function(a, b) {
                const aOrder = self[a].order;
                const bOrder = self[b].order;
                if ( aOrder < bOrder )
                    return -1;
                else if ( aOrder > bOrder )
                    return 1;
                else
                    return ( (a < b)? -1: 1 );
            });
            return keys;
        }
    }).register();

    function createItemsCollection(itemType) {
        return Class.create(`${itemType.name}s`, Items, {
            initialize: function($super) {
                $super(itemType);
            }
        }).register();
    }

    addToExports('createItemsCollection', createItemsCollection);
    addToExports('Item', Item);  
    addToExports('Items', Items);   
    addToExports('CustomObject', CustomObject);
    addToExports('CustomDictionary',CustomDictionary);    
    addToExports('STATE_NOT_CHANGED', STATE_NOT_CHANGED);
    return exports; 
});
