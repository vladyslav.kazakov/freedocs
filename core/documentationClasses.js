define(['../vendor/lodash', './utils.js', './errors.js', './class.js', './BaseClasses.js'], function(_, utils, errors, Class, BaseClasses) {
    'use strict';

    const exports = {};

    function addToExports(name, value) {
        Object.defineProperty(exports, name, {
            value: value
        });
    }

    const handlers = new Map();
    function addHandler(name, handler) {
        handlers.set(name, handler);
    }    

    const statistics = Object.create(null);
    const Documentation = Class.declare('Documentation');
    const DocumentationVersion = Class.declare('DocumentationVersion');
    const Article = Class.declare('Article');
    const Navigation = Class.declare('Navigation');
    const Element = Class.declare('Element');

    const DocumentationVersions = BaseClasses.createItemsCollection(DocumentationVersion);
    Documentation.define(BaseClasses.CustomObject, {
        initialize: function ($super) {
            $super();
            this.definePrimitiveProperty('title', 'string');
            this.defineObject('versions', new DocumentationVersions());
        },

        doDeserialize: function($super, object) {
            $super(object);
            calculate(this);

            function calculate(object) {
                get(object);
                _.forEach(object, function(value) {
                    if ( _.isObjectLike(value) )
                        calculate(value);
                });
            }

            function get(instance) {
                const name = instance.constructor.name;
                if ( !utils.assigned(statistics[name]) ) {
                    statistics[name] = [];
                    statistics[name].push(instance);
                    return instance;
                }
                for (let obj of statistics[name] )
                    if ( _.isEqual(instance, obj) )
                        return obj;

                statistics[name].push(instance);
                return instance;

            }
        }
    }).register();

    const TemplatesGroups = Class.declare('TemplatesGroups');
    DocumentationVersion.define(BaseClasses.Item, {
        initialize: function($super) {
            $super();
            this.defineObject('articles', new BaseClasses.CustomDictionary(Article));
            this.defineObject('navigation', new Navigation());
        },

        doDeserialize: function($super, object) {
            const self = this;
            const instances = Object.create(null);
            let templates;
            if ( utils.assigned(object.templates) ) {
                templates = new TemplatesGroups();
                templates.deserialize(object.templates);
                _.forEach(templates.getObjectTypes(), function(value, key) {
                   instances[key] = [];
                });
                // Class.setObserver(function(instance) {
                    // if ( isInheritsFrom(instance, CustomObject) && (instance.type in instances) )
                        // instances[instance.type].push(instance);
                // });
            }

            _.forEach(object, function(value, key) {
                if ( (key === 'custom') || (key === 'templates') )
                    return;
                const handler = handlers.get(key);
                if ( utils.assigned(handler) )
                    handler(self, value);
            });

            if ( utils.assigned(object.custom) )
                $super(object.custom);
            if ( utils.assigned(templates) ) {
                templates.forEach(function(group) {
                    group.list.forEach(function(template) {
                        instances[group.objectType].forEach(function(instance) {
                            if ( _.isMatch(instance, template.condition) )
                                instance.deserialize(template.transform);
                        });
                    });
                });
            }
        },

        doDiff: function($super, object) {
            const diff = $super(object);
            if ( this === object )
                return diff;

            const states = Object.create(null);
            _.forEach(diff.articles, function(article, id) {
                states[id] = article.state;
            });
            
            _.forEach(diff.navigation.sections, function(section) {
               _.forEach(section.tags, function(tag) {
                    _.forEach(tag.articles, function(article, id) {
                        const state = states[id];
                        if ( state !== BaseClasses.STATE_NOT_CHANGED )
                            article.articleState = state;
                    });
                });
            });
            return diff;
        }
    }).register();

    const CustomArray = Class.create('CustomArray', Array, {
        initialize: function(itemType = BaseClasses.AbstractObject) {
            if ( !_.isFunction(itemType) || !itemType.isClassInheritsFrom(itemType, BaseClasses.AbstractObject) )
                throw new errors.CustomError('CustomArray', 'initialize', `incorrect itemType [${utils.getType(itemType)}]`);

            Object.defineProperty(this, 'itemType', {value: itemType});
        },

        deserialize: function(array) {
            try {
                if ( !_.isArray(array) )
                    throw new IncorrectParameter();
                this.doDeserialize(array);
                return this;
            } catch(e) {
                errors.raiseCatchError(this, 'deserialize', e);
            }
        },

        doDeserialize: function(array) {
            const self = this;
            try {
                if ( self.length !== 0 )
                    throw new Error('self.length !== 0');
                array.forEach(function(item) {
                   self.push((new self.itemType()).deserialize(item));
                });
            } catch(e) {
                errors.raiseCatchError(this, 'doDeserialize', e);
            }
        }
    }).register();

    const TemplateBlock = Class.create('TemplateBlock', BaseClasses.AbstractObject, {
        initialize: function() {

        },

        doDeserialize: function(object) {
            const self = this;
            _.forEach(object, function(value, key) {
                if ( !isPrimitiveType(typeof(value)) )
                    throw new errors.CustomError('TemplateBlock', 'doDeserialize', `non-primitive type detected, key [${key}], value [${value}]`);
                self.defineConstant(key, _.clone(value));
            });
        }
    }).register();

    const Template = Class.create('Template', BaseClasses.CustomObject, {
        initialize: function() {
            this.defineObject('condition', new TemplateBlock());
            this.defineObject('transform', new TemplateBlock());
        },

        doDeserialize: function($super, template) {
            return;
        }
    }).register();

    const TemplatesGroup = Class.create('TemplatesGroup', BaseClasses.CustomObject, {
        initialize: function() {
            this.definePrimitiveProperty('objectType', 'string');
            this.defineObject('list', new CustomArray(Template));
        },

        doDeserialize: function($super, template) {
            try {
                $super(template);
                Class.getClassByName(this.objectType);
            } catch(e) {
                errors.raiseCatchError(this, 'doDeserialize', e);
            }
        }
    }).register();

    TemplatesGroups.define(CustomArray, {
        initialize: function($super) {
            $super(TemplatesGroup);
        },

        getObjectTypes: function() {
            const hash = Object.create(null);
            this.forEach(function(group) {
               hash[group.objectType] = 1;
            });
            return hash;
        }
    }).register();

    const Elements = Class.declare('Elements');
    Article.define(BaseClasses.CustomObject, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('title', 'string');
            this.defineObject('body', new Elements());
        }
    }).register();

    Elements.define(BaseClasses.Items, {
        initialize: function($super) {
            $super(Element);
            Object.defineProperty(this, 'sequence', {
                configurable: false,
                enumerable: false,
                writable: true,
                value: 0
            });
        },
        add: function($super, index, itemType) {
            const item = $super(index, itemType);
            item.order = this.sequence++;
            return item;
        }
    }).register();

    const NavigationItem = Class.create('NavigationItem', BaseClasses.Item, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('title', 'string');
        }
    }).register();

    const Section = Class.declare('Section');
    const Sections = BaseClasses.createItemsCollection(Section);
    Navigation.define(BaseClasses.CustomObject, {
        initialize: function($super) {
            $super();
            this.defineObject('sections', new Sections());
        }
    }).register();

    const Tag = Class.declare('Tag');
    const Tags = BaseClasses.createItemsCollection(Tag);
    Section.define(NavigationItem, {
        initialize: function($super) {
            $super();
            this.defineObject('tags', new Tags());
        }
    }).register();

    const NavigationItems = BaseClasses.createItemsCollection(NavigationItem);
    Tag.define(NavigationItem, {
        initialize: function($super) {
            $super();
            this.defineObject('articles', new NavigationItems());
        }
    }).register();

    Element.define(BaseClasses.Item, {
        doDeserialize: function($super, object) {
            if ( this.constructor === Element )
                throw new errors.CustomError('Element', 'doDeserialize', 'unsupported');
            $super(object);
        }
    }).register();

    const ElementDescription = Class.create('ElementDescription', Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('text', 'string');
        }
    }).register(true);

    const ElementParam = Class.declare('ElementParam');
    const ElementParams = BaseClasses.createItemsCollection(ElementParam);
    const ElementParamsGroup = Class.create('ElementParamsGroup', Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('title', 'string');
            this.definePrimitiveProperty('hasField', 'boolean', true);
            this.definePrimitiveProperty('hasDescription', 'boolean', true);
            this.defineObject('params', new ElementParams());
        }
    }).register(true);

    const ElementParamType = Class.declare('ElementParamType');
    ElementParam.define(Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('description', 'string');
            this.definePrimitiveProperty('in', 'string');
            this.definePrimitiveProperty('required', 'boolean', false);
            this.defineObject('paramType', new ElementParamType());
        }
    }).register();

    const ElementAllowedValue = Class.declare('ElementAllowedValue');
    const ElementAllowedValues = BaseClasses.createItemsCollection(ElementAllowedValue);
    ElementParamType.define(Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('name', 'string');
            this.definePrimitiveProperty('format', 'string');
            this.definePrimitiveProperty('defaultValue');
            this.definePrimitiveProperty('ref', 'string');
            this.definePrimitiveProperty('refName', 'string');
            this.definePrimitiveProperty('pattern', 'string');
            this.defineObject('allowedValues', new ElementAllowedValues());
        }
    }).register();

    ElementAllowedValue.define(Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('description', 'string');
        }
    }).register(true);

    const ElementQueryMethod = Class.create('ElementQueryMethod', Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('method', 'string');
            this.definePrimitiveProperty('url', 'string');
        }
    }).register(true);

    const ElementInheritor = Class.declare('ElementInheritor');
    const ElementInheritors = Class.create('ElementInheritors', Element, {
        initialize: function($super) {
            $super();
            this.defineObject('list', new BaseClasses.Items(ElementInheritor));
        }
    }).register(true);

    ElementInheritor.define(Element, {
        initialize: function($super) {
            $super();
            this.definePrimitiveProperty('ref', 'string');
            this.definePrimitiveProperty('refName', 'string');
            this.definePrimitiveProperty('summary', 'string');
        }
    }).register();

    addToExports('addHandler', addHandler);
    addToExports('Documentation', Documentation);
    addToExports('DocumentationVersion', DocumentationVersion);
    addToExports('Article', Article);
    addToExports('Element', Element);
    addToExports('ElementParamType', ElementParamType);
    addToExports('ElementDescription', ElementDescription);
    addToExports('ElementQueryMethod', ElementQueryMethod);
    addToExports('ElementParamsGroup', ElementParamsGroup);
    addToExports('ElementInheritors', ElementInheritors);
    return exports; 
});