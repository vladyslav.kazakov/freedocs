define(['../vendor/lodash.js'], function(_) {
    'use strict';
    const exports = {};

    function addToExports(name, value) {
        Object.defineProperty(exports, name, {
            value: value
        });
    }

    class CustomError extends Error {
        constructor(type, method, message) {
            // debugger;
            if ( _.isNull(type) || _.isUndefined(type) )
                type = '';
            else if ( _.isObjectLike(type) )
                type = `${type.constructor.name}`;

            if ( _.isFunction(method) )
                method = `${method.name}`;
            else if ( method !== '' )
                method = `${method}`;

            if ( (type === '') && (method === '') )
                super(message);
            else if ( (type === '') || (method === '') )
                super(`${type}${method}: ${message}`);
            else
                super(`${type}.${method}: ${message}`);

            Object.defineProperty(this, 'name', {value: this.constructor.name});
            if ( !_.isUndefined(Error.captureStackTrace) ) { 
                // debugger;
                Error.captureStackTrace(this, this.constructor);
            } else
                this.stack = (new Error()).stack;
        }
    }

    class CatchError extends CustomError {
        constructor(type, method, error, args = '') {
            let message;
            if ( error instanceof CatchError )
                message = args + error.message.replace(/\n/g, '\n    ');
            else

     message = ( (args !== '') ? `${args}, `: '' ) + `message [${error.message}]`;
            super(type, method, message);
            this.message = '\n' + this.message;
        }
    }

    function raiseError(type, method, message) {
        throw new CustomError(type, method, message);
    }

    function raiseCatchError(type, method, error, args) {
        throw new CatchError(type, method, error, args);
    }

    function raiseInvalidArgument(argument = '', method = '', type = null) {
        let message;
        if ( argument !== '' )
            message = `incorrect argument "${argument}"`;
        else
            message = `incorrect argument`;
        raiseError(type, method, message);
    }

    addToExports('raiseError', raiseError);
    addToExports('raiseCatchError', raiseCatchError);
    addToExports('raiseInvalidArgument', raiseInvalidArgument);
    addToExports('CustomError', CustomError);
    return exports;
});