define(['../vendor/lodash.js', './errors.js'], function(_, Errors) {
    'use strict';
    const exports = {};

    function addToExports(name, value) {
        Object.defineProperty(exports, name, {
            value: value
        });
    }

    function defineMethod(instance, name, f) {
        if ( !_.isFunction(f) )
            Errors.raiseInvalidArgument('f');
        Object.defineProperty(instance, name, {
            value: f
        });
    }

    function assigned(value) {
        return !_.isUndefined(value);
    }

    function isPrimitiveType(type) {
        return ( (type === 'string') || (type === 'number') || (type === 'boolean') );
    }

    function getType(variable) {
        // debugger;
        if ( _.isFunction(variable) )
            return variable.name;
        if ( _.isObjectLike(variable) && assigned(variable.constructor) )
            return variable.constructor.name;
        return typeof(variable);
    }

    addToExports('defineMethod', defineMethod);
    addToExports('assigned', assigned);
    addToExports('isPrimitiveType', isPrimitiveType);
    addToExports('getType', getType);
    return exports;
});