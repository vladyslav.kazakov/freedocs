define(['../vendor/lodash', './utils.js', './errors.js'], function(_, utils, errors) {
    'use strict';

    const exports = {};

    function addToExports(name, value) {
        Object.defineProperty(exports, name, {
            value: value
        });
    }

    function getFirstArgumentName(f) {
        return ( f.toString().match(/^[\s\(]*function[^(]*\(([^),]*)[\),]/)[1]
            .replace(/\s+/g, '')
        );
    }

    function defineProperty(instance, name, value) {
        Object.defineProperty(instance, name, {
            value: value
        });
    }

    function checkName(name) {
        if ( !_.isString(name) || (name === '') )
            errors.raiseError(null, checkName, 'incorrect name');
    }

    function checkClassName(name) {
        checkName(name);
        if ( name !== _.upperFirst(name) )
            errors.raiseError(null, checkClassName, 'incorrect name');
    }

    function getSetterName(name) {
        return 'set' + _.capitalize(name);
    }


    function checkPropertyName(name) {
        checkName(name);
        if ( name !== _.lowerFirst(name) )
            throw new Error('incorrect name');
    }

    function checkValue(type, value) {
        return ( _.isNull(value) || (typeof(value) === type) || (!utils.assigned(type) && isPrimitiveType(typeof(value))) );
    }

    function setValueAndCheck(self, propertyName, propertyType, afterSet, value) {
        if ( !checkValue(propertyType, value) ) {
            throw new errors.CustomError(self.constructor.name, getSetterName(propertyName), `incorrect value, valueType [${typeof(value)}], propertyType [${utils.getType(propertyType)}]`);
        }
        setValue(self, propertyName, value);
        if ( utils.assigned(afterSet) ) {
            // debugger;
            afterSet.call(self, value);
        }
    }

    function setValue(self, propertyName, value) {
        self.__storage__[propertyName] = value;
    }

    function getValue(self, propertyName) {
        return self.__storage__[propertyName];
    }

    const __classes__ = new Map();
    const __namedClasses__ = new Map();

    class ClassDescriptor {
        constructor(name) {
            checkClassName(name);
            defineProperty(this, 'name', name);
        }

        define(classConstructor, ancestor, methods) {
            if ( this.isDefined() )
                throw new Error('already defined');
            defineProperty(this, 'classConstructor', classConstructor);
            this.setAncestor(ancestor, classConstructor);
            this.setMethods(methods);
            return classConstructor;
        }

        setAncestor(ancestor, classConstructor) {
            if ( _.isNull(ancestor) )
                ancestor = Object;
            defineProperty(classConstructor, 'prototype', Object.create(ancestor.prototype));
            utils.defineMethod(classConstructor.prototype, 'constructor', this.classConstructor);
            defineProperty(this, 'ancestor', ancestor);
            if ( utils.assigned(__classes__.get(ancestor)) )
                return;

            utils.defineMethod(classConstructor.prototype, 'defineObject', function defineObject(name, value) {
                try {
                    if ( !_.isObjectLike(value) )
                        errors.raiseInvalidArgument('value', defineObject);
                    Object.defineProperty(this, name, {
                        enumerable: true,
                        get: getValue.bind(null, this, name)
                    });
                    this.__storage__[name] = value;
                } catch(e) {
                    errors.raiseCatchError(this, defineObject, e, `name [${name}]`);
                }
            });

            utils.defineMethod(classConstructor.prototype, 'definePrimitiveProperty', function definePrimitiveProperty(name, type, value = null, setter = undefined) {
                const propertyDescriptor = {
                    enumerable: true,
                    get: getValue.bind(null, this, name),
                    set: setValueAndCheck.bind(null, this, name, type, setter)
                };
                Object.defineProperty(this, name, propertyDescriptor);
                this[name] = value;
                utils.defineMethod(this, `set${_.upperFirst(name)}`, function(value) {
                    this[name] = value;
                    return this;
                });
            });
        }

        setMethods(methods) {
            if ( _.isNull(methods) )
                return;
            if ( !_.isObjectLike(methods) )
                errors.raiseInvalidArgument('methods');
            const self = this;
            _.forEach(methods, function(body, name) {
                if ( !_.isFunction(body) )
                    errors.raiseError(self, self.setMethods, `non-function detected [${name}]`);
                utils.defineMethod(self.classConstructor.prototype, name, self.createFunction(name, body));
            });
        }

        createFunction(name, body) {
            const firstArgumentName = getFirstArgumentName(body);
            if ( firstArgumentName === '$super' ) {
                const $super = this.ancestor.prototype[name];
                if ( !utils.assigned($super) )
                    errors.raiseError(this, this.createFunction, `function not found, [${name}], [${this.ancestor.name}]`);
                return function() {
                    const args = [].slice.call(arguments);
                    args.unshift($super.bind(this));
                    return body.apply(this, args);
                };
            }
            return body;
        }

        isRegistered() {
            return Object.isFrozen(this.classConstructor.prototype);
        }

        isDefined() {
            return utils.assigned(this.classConstructor);
        }

        initInstance(instance, args) {
            if ( !this.isDefined() )
                errors.raiseError(this, this.initInstance, `class undefined [${this.name}]`);
            if ( !this.isRegistered() )
                errors.raiseError(this, this.initInstance, `class unregistered [${this.name}]`);
            defineProperty(instance, '__storage__', {});
            const init = this.classConstructor.prototype.initialize;
            if ( utils.assigned(init) )
                init.apply(instance, args);
        }
    }

    function declareClass(name) {
        const classDescriptor = new ClassDescriptor(name);

        /*jslint evil: true */
        const constructor = new Function('classDescriptor', 'raiseCatchError',
            `return function ${name}() {\n` +
            `    try {\n` +
            `        classDescriptor.initInstance(this, [].slice.call(arguments));\n` +
            `    } catch(e) {\n` +
            `        raiseCatchError(classDescriptor.name, 'create', e);\n` +
            `    }\n` +
            `};`
        )(classDescriptor, errors.raiseCatchError);
        __classes__.set(constructor, classDescriptor);

        utils.defineMethod(constructor, 'register', function register(isReserveName = false) {
            try {
                Object.freeze(constructor.prototype);
                if ( isReserveName ) {
                    if ( utils.assigned(__namedClasses__.get(constructor.name)) )
                        throw new Error('name already reserved');
                    __namedClasses__.set(constructor.name, constructor);
                }
                return constructor;
            } catch(e) {
                errors.raiseCatchError(classDescriptor.name, register, e);
            }
        });

        utils.defineMethod(constructor, 'define', function define(ancestor = null, methods = null) {
            try {
                return classDescriptor.define(constructor, ancestor, methods);
            } catch(e) {
                errors.raiseCatchError(classDescriptor.name, define, e);
            }
            return this;
        });

        utils.defineMethod(constructor, 'isClassInheritsFrom', function isClassInheritsFrom(ancestor) {
            if ( !classDescriptor.isDefined() )
                errors.raiseError(classDescriptor.name, isClassInheritsFrom, `class undefined [${constructor.name}]`);
            return ( (constructor === ancestor) || (constructor.prototype instanceof ancestor) );
        });

        return constructor;
    }

    function declare(name) {
        try {
            return declareClass(name);
        } catch(e) {
            errors.raiseCatchError('Class', declare, e, `name [${name}]`);
        }
    }

    function create(name, ancestor = null, methods = null) {
        try {
            return declareClass(name).define(ancestor, methods);
        } catch(e) {
            errors.raiseCatchError('Class', create, e, `name [${name}]`);
        }
    }

    function getClassByName(name) {
        // debugger;
        try {
            if ( !_.isString(name) )
                throw new Error('incorrect class name');
            const constructor = __namedClasses__.get(name);
            if ( !utils.assigned(constructor) )
                throw new Error('unknown class');
            return constructor;
        } catch(e) {
            //debugger;
            throw new errors.raiseCatchError('Class', 'getClassByName', e, `name [${name}]`);
        }
    }

    addToExports('declare', declare);
    addToExports('create', create);
    addToExports('getClassByName', getClassByName);
    addToExports('defineProperty', defineProperty);
    return exports;
});