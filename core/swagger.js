define(['../vendor/lodash', './documentationClasses.js', './utils.js', './errors.js', './baseClasses.js'], function(_, DocumentationClasses, utils, errors, BaseClasses) {
    'use strict';

    function swagger(documentationVersion, object) {
        const articles = documentationVersion.articles;
        const apiSection = documentationVersion.navigation.sections.getOrAdd('api').setTitle('API');
        createApiArticles();

        const modelSection = documentationVersion.navigation.sections.getOrAdd('model').setTitle('MODELS');
        createModelArticles();

        function createApiArticles() {
            try {
                _.forEach(object.paths, function(service, serviceName) {
                    createApiArticle(service, serviceName);
                });
            } catch(e) {
                throw new errors.raiseCatchError('swagger', 'createApiArticles', e);
            }
        }

        function createApiArticle(service, serviceName) {
            const serviceID = getServiceID(serviceName);
            const queryMethod = Object.keys(service)[0];
            service = service[queryMethod];
            const article = articles.add(serviceID).setTitle(service.summary);
            article.body.add('description', DocumentationClasses.ElementDescription).setText(service.description);
            article.body.add('queryMethod', DocumentationClasses.ElementQueryMethod).setMethod(queryMethod).setUrl(serviceName);
            if ( service.tags ) {
                  _.forEach(service.tags, function(tagName) {
                  apiSection.tags.getOrAdd(getServiceID(tagName))
                        .setTitle(tagName)
                        .articles.add(serviceID)
                            .setTitle(service.summary);
                });
            }

            if ( service.parameters ) {
                const groupID = 'Request:';
                const group = article.body.add(groupID, DocumentationClasses.ElementParamsGroup).setTitle(groupID);
                _.forEach(service.parameters, function(definition) {
                    const param = group.params.add(definition.name)
                        .setRequired(definition.required)
                        .setIn(definition.in)
                        .setDescription(definition.description);
                    const type = createParameterType(definition.type, definition.schema, definition.format, definition.pattern);
                    setElementParamType(param, type);
                });
            }

            if ( service.responses ) {
                _.forEach(service.responses, function(response, code) {
                    const groupID = 'Response ' + code + ':';
                    const group = article.body.add(groupID, DocumentationClasses.ElementParamsGroup).setTitle(groupID);
                    group.hasField = false;
                    const param = group.params.add('')
                        .setRequired(true)
                        .setDescription(response.description);
                    const type = createParameterType(response.type, response.schema);
                    setElementParamType(param, type);
                });
            }
        }

        function createModelArticles() {
            try {
                const models = {};
                _.forEach(Object.keys(object.definitions), function(modelName) {
                    addModel(object.definitions, modelName, models);
                });
                _.forEach(models, function(model, modelName) {
                    const modelID = getModelID(modelName);
                    const article = articles.add(modelID).setTitle(modelName);
                    article.body.add('description', DocumentationClasses.ElementDescription).setText(model.description || null);
                    if ( utils.assigned(model.inheritors) ) {
                        const list = article.body.add('inheritors', DocumentationClasses.ElementInheritors).list;
                        _.forEach(model.inheritors, function(name) {
                            list.add(name)
                                .setRef(getModelID(name))
                                .setRefName(name)
                                .setSummary(models[name].description || null);
                        });
                    }
                    const tag_others = 'others';
                    _.forEach(model.tags, function(name) {
                        let tagName = tag_others;
                        // if exists inheritors or ancestors
                        if ( utils.assigned(model.inheritors) || (model.allOf.length > 1) )
                            tagName = name;
                        const articleRef = modelSection.tags.getOrAdd(getModelID(tagName))
                            .setTitle(tagName)
                            .articles.add(modelID).setTitle(modelName);
                        if ( utils.assigned(model.inheritors) )
                            articleRef.setOrder(0);
                    });
                    const tag = modelSection.tags[tag_others];
                    if ( utils.assigned(tag) )
                        tag.setOrder(10);
                    _.forEach(model.allOf, function(paramsGroup) {
                        const groupID = paramsGroup.modelName + ' fields:';
                        const group = article.body.add(groupID, DocumentationClasses.ElementParamsGroup).setTitle(groupID);
                        _.forEach(paramsGroup.properties, function(definition, name) {
                            const param = group.params.add(name)
                                .setRequired(definition.required || null)
                                .setIn(definition.in || null)
                                .setDescription(definition.description || null);
                            setElementParamType(param, definition.type);
                        });
                    });
                });
            } catch(e) {
                throw new errors.raiseCatchError('', 'swagger.createModelArticles', e);
            }
        }

        function setElementParamType(param, type) {
            param.paramType
                .setName(type.name || null)
                .setFormat(type.format || null)
                .setDefaultValue(type.defaultValue || null)
                .setPattern(type.pattern || null);
            if ( type.allowedValues ) {
                const allowedValues = param.paramType.allowedValues;
                _.forEach(type.allowedValues, function(value) {
                    allowedValues.add(value);
                });
            }
            if ( type.modelRef )
                param.paramType.setRef(type.modelRef.ref).setRefName(type.modelRef.name);
        }

        function addModel(definitions, modelName, models) {
            if ( modelName in models )
                return;
                // create new model
                let model = definitions[modelName];
                let allOf = [];
                let tags = [];
                if ( !utils.assigned(model.allOf) ) {
                    model = {
                        allOf: [model]
                    };
                    tags = [modelName];
                }
                models[modelName] = model;

                _.forEach(model.allOf, function(modelDefinition) {
                    if ( modelDefinition.$ref )
                        copyFromAncestor(modelDefinition.$ref);
                    else
                        prepareModel(modelDefinition);
                });
                model.allOf = allOf;
                model.tags = tags;

                function addInheritorToAncestor(inheritorName, ancestorName) {
                    const ancestor = models[ancestorName];
                    if ( !ancestor.inheritors )
                        ancestor.inheritors = [];
                    ancestor.inheritors.push(inheritorName);
                }

                function copyFromAncestor(modelRef) {
                    const ancestorName = modelRef.replace('#/definitions/', '');
                    addModel(definitions, ancestorName, models);
                    const ancestor = models[ancestorName];
                    allOf = allOf.concat(ancestor.allOf);
                    tags = tags.concat(ancestor.tags);
                    _.forEach(allOf, function(ancestorParameters) {
                        addInheritorToAncestor(modelName, ancestorParameters.modelName);
                    });
                }

                function prepareModel(modelDefinition) {
                    const properties = (modelDefinition.properties || []);
                    allOf.push({modelName: modelName, properties: properties});
                    _.forEach(properties, function(parameter, parameterName) {
                        parameter.type = createParameterType(parameter.type, parameter, parameter.format, parameter.pattern);
                    });
                    if ( modelDefinition.required ) {
                        _.forEach(modelDefinition.required, function(propertyName) {
                            for (let i = 0; i < allOf.length; ++i) {
                                const property = allOf[i].properties[propertyName];
                                if ( property ) {
                                    property.required = true;
                                    break;
                                }
                            }
                        });
                    }
                    if ( modelDefinition.description )
                        model.description = modelDefinition.description;
                }
        }

        function getServiceID(name) {
            return `api-${name}`;
        }

        function getModelID(name) {
            return `model-${name}`;
        }

        function getModelName(modelName) {
            return modelName.replace('#/definitions/', '');
        }

        function createParameterType(type, schema, format, pattern) {
            const parameterType = {};
            if ( type )
                parameterType.name = type;
            if ( schema ) {
                if ( schema.type )
                    parameterType.name = schema.type;
                let ref = schema.$ref;
                if ( ref )
                    delete schema.$ref;
                if ( schema.items && (schema.type === 'array') ) {
                    parameterType.name = 'array of ';
                    ref = schema.items.$ref;
                    delete schema.type;
                    delete schema.items.$ref;
                }
                if ( ref )
                    parameterType.modelRef = createModelRef(getModelName(ref));
                if ( 'default' in schema )
                    parameterType.defaultValue = schema.default;
                if ( schema.enum )
                    parameterType.allowedValues = schema.enum;
                if ( ('minLength' in schema) && ('maxLength' in schema) ) {
                    parameterType.format = ' to ' + schema.maxLength + ' chars';
                    if ( schema.minLength === 0 )
                        parameterType.format = 'up' + parameterType.format;
                    else
                        parameterType.format = schema.minLength + parameterType.format;
                }
            }
            if ( format )
                parameterType.format = format;
            if ( pattern )
                parameterType.pattern = pattern;
            return parameterType;
        }

        function createModelRef(name) {
            return {
                ref: getModelID(name),
                name: name
            };
        }
    }

    return swagger;
});
