define({
  "title": "EWA.API",
  "versions": {
    "2016.04": {
      "swagger": {
        "swagger" : "2.0",
        "info" : {
          "version" : "2016.04",
          "title" : "EWA.REST-API.DOCS"
        },
        "host" : "test.ewa.ua",
        "basePath" : "/evo/api/v201604",
        "tags" : [ {
          "name" : "Договоры"
        }, {
          "name" : "Пользователи"
        }, {
          "name" : "Тарифы"
        } ],
        "schemes" : [ "https" ],
        "paths" : {
          "/contract/save" : {
            "post" : {
              "tags" : [ "Договоры" ],
              "summary" : "Сохранение договора",
              "description" : "Доступно для CAN_EDIT_CONTRACT<br/>Возвращает сохраненный договор.",
              "operationId" : "save",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "in" : "body",
                "name" : "body",
                "description" : "Сохраняемый договор",
                "required" : false,
                "schema" : {
                  "$ref" : "#/definitions/Contract"
                }
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/Contract"
                  }
                }
              }
            }
          },
          "/tariff/choose/greencard" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Калькулятор \"Зеленая карта\"",
              "description" : "Подбор тарифа для договоров \"Зеленой карты\"",
              "operationId" : "chooseGreenCardTariff",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "salePoint",
                "in" : "query",
                "description" : "Идентификатор точки продаж для которой (и ее подчиненных) отбираются тарифы. Если не задан, то берется идентификатор точки пользователя",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "dateFrom",
                "in" : "query",
                "description" : "Начальная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "area",
                "in" : "query",
                "description" : "Территория покрытия будущего договора",
                "required" : false,
                "type" : "string",
                "enum" : [ "BMR", "EUROPE" ]
              }, {
                "name" : "type",
                "in" : "query",
                "description" : "Тип транспортного средства",
                "required" : false,
                "type" : "string",
                "enum" : [ "A", "B", "C", "E", "F" ]
              }, {
                "name" : "period",
                "in" : "query",
                "description" : "Период действия договора",
                "required" : false,
                "type" : "string",
                "enum" : [ "FIFTEEN_DAYS", "ONE_MONTH", "TWO_MONTHS", "THREE_MONTHS", "FOUR_MONTHS", "FIVE_MONTHS", "SIX_MONTHS", "SEVEN_MONTHS", "EIGHT_MONTHS", "NINE_MONTHS", "TEN_MONTHS", "ELEVEN_MONTHS", "YEAR" ]
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/GreenCardTariffChoose"
                    }
                  }
                }
              }
            }
          },
          "/tariff/choose/policy" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Калькулятор ОСАГО\"",
              "description" : "Подбор тарифа для договоров ОСАГО",
              "operationId" : "choosePolicyTariff",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "salePoint",
                "in" : "query",
                "description" : "Идентификатор точки продаж для которой (и ее подчиненных) отбираются тарифы.Если не задан, то берется идентификатор точки пользователя",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "customerCategory",
                "in" : "query",
                "description" : "Категория контрагента",
                "required" : false,
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              }, {
                "name" : "franchise",
                "in" : "query",
                "description" : "Франшиза",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "bonusMalus",
                "in" : "query",
                "description" : "Бонус-малус",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "discount",
                "in" : "query",
                "description" : "Скидка",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "taxi",
                "in" : "query",
                "description" : "Признак. Авто используется как такси",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "autoCategory",
                "in" : "query",
                "description" : "Категория авто",
                "required" : false,
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              }, {
                "name" : "zone",
                "in" : "query",
                "description" : "Зона регистрации авто",
                "required" : false,
                "type" : "integer",
                "format" : "int32"
              }, {
                "name" : "dateFrom",
                "in" : "query",
                "description" : "Начальная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "dateTo",
                "in" : "query",
                "description" : "Конечная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "usageMonths",
                "in" : "query",
                "description" : "Месяцы неиспользования",
                "required" : false,
                "type" : "integer",
                "format" : "int32"
              }, {
                "name" : "driveExp",
                "in" : "query",
                "description" : "Признак. Стаж вождения менее трех лет",
                "required" : false,
                "type" : "boolean"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffPolicyChoose"
                    }
                  }
                }
              }
            }
          },
          "/user" : {
            "get" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Список пользователей",
              "description" : "Доступно для SUPER_ADMIN, CAN_EDIT_USERS",
              "operationId" : "list",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Отбирать пользователей с ФИО начинающимся с",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "insurerId",
                "in" : "query",
                "description" : "фильтр по id страховщика которому принадлежат пользователи.<ul><li>Если пустой и пользователь SUPER_ADMIN, то фильтр не применяется</li><li>Если пустой и пользователь не SUPER_ADMIN, то фильтруется по страхователю текущего пользователя</li><li>Если задан и пользователь не SUPER_ADMIN и не совпадает со страхователем которому принадлежит текущий пользователь, то FORBIDDEN.</li></ul>",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "id точки продаж",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/User"
                    }
                  }
                }
              }
            }
          },
          "/user/current" : {
            "get" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Пролучение информации о текущем пользователе",
              "description" : "",
              "operationId" : "currentUser",
              "produces" : [ "application/json" ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/CurrentUserInfo"
                  }
                }
              }
            }
          },
          "/user/login" : {
            "post" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Вход в систему",
              "description" : "",
              "operationId" : "login",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "email",
                "in" : "formData",
                "description" : "email пользователя",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "password",
                "in" : "formData",
                "description" : "SHA1-дайджест пароля пользователя",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "asuser",
                "in" : "formData",
                "description" : "Войти как. ТОЛЬКО для суперпользователей",
                "required" : false,
                "type" : "string"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/CurrentUserInfo"
                  }
                }
              }
            }
          },
          "/user/logout" : {
            "put" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Выход из системы",
              "description" : "",
              "operationId" : "logout",
              "produces" : [ "application/json" ],
              "responses" : {
                "default" : {
                  "description" : "successful operation"
                }
              }
            }
          },
          "/user/restorePassword" : {
            "put" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Восстановление пароля",
              "description" : "",
              "operationId" : "restorePassword",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "email",
                "in" : "query",
                "description" : "email пользователя",
                "required" : false,
                "type" : "string"
              } ],
              "responses" : {
                "default" : {
                  "description" : "successful operation"
                }
              }
            }
          },
          "/user/save" : {
            "post" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Сохранение пользователя",
              "description" : "Если не CAN_EDIT_INSURERS, и пользователь принадлежит страховщику отличного от того которому принадлежит текущий пользователь либо при смене страховщика, то вернется FORBIDDEN",
              "operationId" : "save",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "in" : "body",
                "name" : "body",
                "description" : "сохраняемый пользователь",
                "required" : false,
                "schema" : {
                  "$ref" : "#/definitions/User"
                }
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/User"
                  }
                }
              }
            }
          },
          "/user/{id}" : {
            "get" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Получение пользователя",
              "description" : "Для всех авторизированных можно получить только информацию о себе, SUPER_ADMIN может получить любого",
              "operationId" : "get",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "id",
                "in" : "path",
                "description" : "id пользователя",
                "required" : true,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/User"
                  }
                }
              }
            }
          }
        },
        "definitions" : {
          "AutoFieldSettings" : {
            "type" : "object",
            "required" : [ "autoCategory", "autoColor", "autoEngineVolume", "autoModel", "autoOwner", "autoRegistrationDocument", "autoRegistrationPlace", "autoRegistrationType", "autoStateNumber", "autoVIN", "autoYear", "creditDocument", "pledgeDocument" ],
            "properties" : {
              "autoModel" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoVIN" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoStateNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationPlace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoYear" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoEngineVolume" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationType" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoColor" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoOwner" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "creditDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "pledgeDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "AutoMaker" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентфикатор"
              },
              "name" : {
                "type" : "string",
                "description" : "Название модели авто",
                "minLength" : 0,
                "maxLength" : 20
              }
            },
            "description" : "Модель авто"
          },
          "AutoModel" : {
            "type" : "object",
            "required" : [ "autoMaker" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентфикатор"
              },
              "autoMaker" : {
                "description" : "Марка автомобиля",
                "$ref" : "#/definitions/AutoMaker"
              },
              "kind" : {
                "type" : "string",
                "description" : "Тип автомобиля",
                "enum" : [ "MOTO", "CAR", "PASSENGER", "FREIGHT", "TRAILER", "AGRICULTURAL", "SPECIAL" ]
              },
              "name" : {
                "type" : "string",
                "description" : "Название модели автомобиля",
                "minLength" : 0,
                "maxLength" : 50
              },
              "categories" : {
                "type" : "array",
                "items" : {
                  "type" : "string",
                  "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
                }
              }
            },
            "description" : "Модель атомобиля"
          },
          "Bank" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "mfo" : {
                "type" : "string",
                "minLength" : 6,
                "maxLength" : 6
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              }
            }
          },
          "Broker" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CompanySimple"
            }, {
              "type" : "object",
              "required" : [ "dateFrom" ],
              "properties" : {
                "evoUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "smsName" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 11,
                  "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 100
                },
                "hotlinePhone" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "homePage" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "paymentOptions" : {
                  "type" : "string"
                },
                "calculationKinds" : {
                  "type" : "string"
                },
                "accounts" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyAccount"
                  }
                },
                "emails" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyEmail"
                  }
                },
                "contractTypes" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ContractType"
                  }
                },
                "dateFrom" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dateTo" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "hasDataHelper" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dataHelperParameters" : {
                  "type" : "string"
                },
                "closePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "exportClosePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                }
              }
            } ]
          },
          "Company" : {
            "type" : "object",
            "required" : [ "country", "dateFrom" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 8,
                "maxLength" : 10,
                "pattern" : "^[0-9]*$"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "exportersAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "evoUrl" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120,
                "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
              },
              "smsName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 11,
                "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
              },
              "address" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "hotlinePhone" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "homePage" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120,
                "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
              },
              "paymentOptions" : {
                "type" : "string"
              },
              "calculationKinds" : {
                "type" : "string"
              },
              "accounts" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CompanyAccount"
                }
              },
              "emails" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CompanyEmail"
                }
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "hasDataHelper" : {
                "type" : "boolean",
                "default" : false
              },
              "dataHelperParameters" : {
                "type" : "string"
              },
              "closePeriod" : {
                "type" : "string",
                "format" : "date-time"
              },
              "exportClosePeriod" : {
                "type" : "string",
                "format" : "date-time"
              }
            }
          },
          "CompanyAccount" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "mfo" : {
                "type" : "string",
                "minLength" : 6,
                "maxLength" : 6
              },
              "bank" : {
                "$ref" : "#/definitions/Bank"
              },
              "account" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 14
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              },
              "defaultAccount" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "CompanyEmail" : {
            "type" : "object",
            "required" : [ "email", "status" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "email" : {
                "type" : "string"
              },
              "status" : {
                "type" : "string",
                "enum" : [ "CONFIRMED", "NOT_CONFIRMED", "SENT" ]
              },
              "defaultEmail" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "CompanySimple" : {
            "type" : "object",
            "required" : [ "country" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 8,
                "maxLength" : 10,
                "pattern" : "^[0-9]*$"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "exportersAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "Contract" : {
            "type" : "object",
            "required" : [ "baseTariff", "brokerDiscountedPayment", "date", "dateFrom", "dateTo", "discount", "multiObject", "payment", "salePoint", "state", "tariff", "urgencyCoefficient", "user" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string"
              },
              "salePoint" : {
                "description" : "точка продаж на которой был реализован договор",
                "$ref" : "#/definitions/SalePointSimple"
              },
              "user" : {
                "description" : "пользователь, внесший договор",
                "$ref" : "#/definitions/UserSimple"
              },
              "created" : {
                "type" : "string",
                "format" : "date-time"
              },
              "stateChanged" : {
                "type" : "string",
                "format" : "date-time"
              },
              "stateChangedBy" : {
                "description" : "пользователь, изменивший состояние договора",
                "$ref" : "#/definitions/UserSimple"
              },
              "tariff" : {
                "description" : "страховой продукт",
                "$ref" : "#/definitions/TariffSimple"
              },
              "number" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 15
              },
              "generalNumber" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "date" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "period" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "customer" : {
                "$ref" : "#/definitions/Person"
              },
              "beneficiary" : {
                "$ref" : "#/definitions/Person"
              },
              "insuranceObject" : {
                "$ref" : "#/definitions/InsuranceObject"
              },
              "insuranceObjects" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/InsuranceObject"
                }
              },
              "baseTariff" : {
                "type" : "number"
              },
              "discount" : {
                "type" : "number"
              },
              "urgencyCoefficient" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              },
              "brokerDiscountedPayment" : {
                "type" : "number"
              },
              "commission" : {
                "type" : "number"
              },
              "insurerAccount" : {
                "$ref" : "#/definitions/CompanyAccount"
              },
              "agents" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractAgent"
                }
              },
              "notes" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 1024
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "SIGNED", "REQUEST", "EMITTED" ]
              },
              "payments" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractPayment"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CustomFieldValue"
                }
              },
              "multiObject" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string"
              }
            },
            "description" : "Базовый класс договора"
          },
          "ContractAgent" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "agent" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "paymentOption" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "calculationKind" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "commission" : {
                "type" : "number"
              }
            }
          },
          "ContractDateToLimit" : {
            "type" : "object",
            "properties" : {
              "value" : {
                "type" : "integer",
                "format" : "int32"
              },
              "datePeriodType" : {
                "type" : "string",
                "enum" : [ "DAYS", "MONTHS" ]
              }
            }
          },
          "ContractPayment" : {
            "type" : "object",
            "required" : [ "created", "date", "payment", "state", "stateChanged" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "date" : {
                "type" : "string",
                "format" : "date-time"
              },
              "created" : {
                "type" : "string",
                "format" : "date-time"
              },
              "stateChanged" : {
                "type" : "string",
                "format" : "date-time"
              },
              "payment" : {
                "type" : "number"
              },
              "number" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "purpose" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 200
              },
              "state" : {
                "type" : "string",
                "enum" : [ "PENDING", "PAID", "CANCELED" ]
              },
              "recipientName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "recipientCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "recipientBankMfo" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 6
              },
              "recipientAccountNumber" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "senderName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "senderCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "senderBankMfo" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 6
              },
              "senderAccountNumber" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              }
            }
          },
          "ContractType" : {
            "type" : "object",
            "required" : [ "active" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 40
              },
              "zeroPadding" : {
                "type" : "integer",
                "format" : "int32"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            },
            "description" : "Тип договора"
          },
          "CurrentUserInfo" : {
            "type" : "object",
            "properties" : {
              "user" : {
                "readOnly" : true,
                "$ref" : "#/definitions/User"
              },
              "sessionId" : {
                "type" : "string",
                "readOnly" : true
              },
              "hasPolicyTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasPolicyRuTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasVclTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasGreenCardTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasTourismTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "customContractTypes" : {
                "type" : "array",
                "readOnly" : true,
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "CustomContract" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "insuranceAmount" ],
              "properties" : {
                "insuranceAmount" : {
                  "type" : "number"
                }
              },
              "description" : "Базовый класс договора"
            } ]
          },
          "CustomDateField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object"
            } ]
          },
          "CustomField" : {
            "type" : "object",
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "description" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 400
              },
              "archive" : {
                "type" : "boolean",
                "default" : false
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "CustomFieldValue" : {
            "type" : "object",
            "required" : [ "calculationKind" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "value" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10000
              },
              "calculationKind" : {
                "type" : "string",
                "enum" : [ "NONE", "TARIFF_MULTIPLIER", "TARIFF_ADDITION", "PAYMENT_MULTIPLIER", "PAYMENT_ADDITION" ]
              },
              "coeff" : {
                "type" : "number"
              }
            }
          },
          "CustomListField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object",
              "properties" : {
                "members" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CustomListFieldMember"
                  }
                }
              }
            } ]
          },
          "CustomListFieldMember" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              }
            }
          },
          "CustomTextField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object",
              "properties" : {
                "multiline" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minLength" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "maxLength" : {
                  "type" : "integer",
                  "format" : "int32",
                  "maximum" : 10000.0
                },
                "mask" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 400
                },
                "defaultValue" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 400
                }
              }
            } ]
          },
          "CustomerFieldSettings" : {
            "type" : "object",
            "required" : [ "address", "birthDate", "code", "documentDate", "documentIssuedBy", "documentNumber", "documentSeries", "mail", "name", "nameFirst", "nameMiddle", "phone" ],
            "properties" : {
              "code" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "allowUndefinedCode" : {
                "type" : "boolean",
                "default" : false
              },
              "codeDefault" : {
                "type" : "string",
                "pattern" : "[0-9]*"
              },
              "name" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "nameDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "nameFirst" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "nameMiddle" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "address" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "addressDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "useAddressHelper" : {
                "type" : "boolean",
                "default" : false
              },
              "phone" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "phoneDefault" : {
                "type" : "string",
                "minLength" : 12,
                "maxLength" : 13
              },
              "mail" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "mailDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "birthDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "birthDateDefault" : {
                "type" : "string",
                "format" : "date-time"
              },
              "documentTypes" : {
                "type" : "array",
                "items" : {
                  "type" : "string",
                  "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
                }
              },
              "documentTypeDefault" : {
                "type" : "string",
                "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
              },
              "documentSeries" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentSeriesDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 4
              },
              "documentNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentNumberDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "documentDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentDateDefault" : {
                "type" : "string",
                "format" : "date-time"
              },
              "documentIssuedBy" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentIssuedByDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "useDocumentIssuedByHelper" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "GreenCard" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "properties" : {
                "vehicleType" : {
                  "type" : "string",
                  "enum" : [ "A", "B", "C", "E", "F" ]
                },
                "coverageArea" : {
                  "type" : "string",
                  "enum" : [ "BMR", "EUROPE" ]
                },
                "verificationCode" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 15
                }
              },
              "description" : "Базовый класс договора"
            } ]
          },
          "GreenCardTariffChoose" : {
            "type" : "object",
            "properties" : {
              "tariff" : {
                "readOnly" : true,
                "$ref" : "#/definitions/TariffGreenCard"
              },
              "payment" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedPayment" : {
                "type" : "number",
                "readOnly" : true
              },
              "commission" : {
                "type" : "number"
              },
              "discountedCommission" : {
                "type" : "number"
              },
              "approx" : {
                "type" : "boolean",
                "default" : false
              },
              "canCreated" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "commissionForEurope" : {
                "type" : "number"
              },
              "commissionForBMR" : {
                "type" : "number"
              }
            }
          },
          "InsuranceObject" : {
            "type" : "object",
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "price" : {
                "type" : "number"
              },
              "insuranceAmount" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CustomFieldValue"
                }
              }
            }
          },
          "InsuranceObjectAuto" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "required" : [ "dontHaveBodyNumber", "dontHaveStateNumber" ],
              "properties" : {
                "category" : {
                  "type" : "string",
                  "description" : "Категория авто",
                  "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
                },
                "model" : {
                  "description" : "Модель авто",
                  "$ref" : "#/definitions/AutoModel"
                },
                "modelText" : {
                  "type" : "string",
                  "description" : "Модель авто текстом",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "bodyNumber" : {
                  "type" : "string",
                  "description" : "Номер кузова (VIN)",
                  "minLength" : 0,
                  "maxLength" : 17
                },
                "dontHaveBodyNumber" : {
                  "type" : "boolean",
                  "description" : "Признак того, что авто не имеет номера кузова",
                  "default" : false
                },
                "stateNumber" : {
                  "type" : "string",
                  "description" : "Госномер авто",
                  "minLength" : 0,
                  "maxLength" : 10
                },
                "dontHaveStateNumber" : {
                  "type" : "boolean",
                  "description" : "Признак того, что авто не имеет госномера",
                  "default" : false
                },
                "registrationPlace" : {
                  "description" : "Место регистрации авто",
                  "$ref" : "#/definitions/Place"
                },
                "outsideUkraine" : {
                  "type" : "boolean",
                  "description" : "Признак того, что авто зарегистрировано за пределами Украины",
                  "default" : false
                },
                "registrationType" : {
                  "type" : "string",
                  "description" : "Тип регистрации авто",
                  "enum" : [ "PERMANENT_WITHOUT_OTK", "PERMANENT_WITH_OTK", "NOT_REGISTERED", "TEMPORARY", "TEMPORARY_ENTRANCE" ]
                },
                "otkDate" : {
                  "type" : "string",
                  "format" : "date-time",
                  "description" : "Дата регистрации. Смысл меняется в зависимости от типа регистрации:<ul><li>игнорируется, если тип \"Постоянная регистрация (без ОТК)\"<li>дата следующего ОТК, если тип \"Постоянная регистрация (c ОТК)\"<li>дата будущей регистрации, если тип \"Без регистрации\"<li>дата окончания регистрации, если тип \"Временная регистрация\"<li>дата выезда, если тип \"Временный вьезд\"</ul>"
                },
                "year" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "engineVolume" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "autoColor" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 50
                },
                "autoOwner" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "autoRegistrationDocument" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "autoCreditDocument" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "autoPledgeDocument" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                }
              }
            } ]
          },
          "InsuranceObjectPerson" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "properties" : {
                "phone" : {
                  "type" : "string"
                },
                "nameLast" : {
                  "type" : "string"
                },
                "nameFirst" : {
                  "type" : "string"
                },
                "nameMiddle" : {
                  "type" : "string"
                },
                "birthDate" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dontHaveCode" : {
                  "type" : "boolean",
                  "default" : false
                },
                "document" : {
                  "$ref" : "#/definitions/PersonDocument"
                },
                "code" : {
                  "type" : "string"
                },
                "email" : {
                  "type" : "string"
                },
                "address" : {
                  "type" : "string"
                },
                "name" : {
                  "type" : "string"
                }
              }
            } ]
          },
          "InsuranceObjectProperty" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "properties" : {
                "description" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "place" : {
                  "$ref" : "#/definitions/Place"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "totalSpace" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "livingSpace" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "floor" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "numberOfFloors" : {
                  "type" : "integer",
                  "format" : "int32",
                  "minimum" : 1.0
                },
                "propertyOwner" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                }
              }
            } ]
          },
          "Insurer" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CompanySimple"
            }, {
              "type" : "object",
              "required" : [ "dateFrom" ],
              "properties" : {
                "evoUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "smsName" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 11,
                  "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 100
                },
                "hotlinePhone" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "homePage" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "paymentOptions" : {
                  "type" : "string"
                },
                "calculationKinds" : {
                  "type" : "string"
                },
                "accounts" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyAccount"
                  }
                },
                "emails" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyEmail"
                  }
                },
                "contractTypes" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ContractType"
                  }
                },
                "dateFrom" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dateTo" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "hasDataHelper" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dataHelperParameters" : {
                  "type" : "string"
                },
                "closePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "exportClosePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "insurerCode" : {
                  "type" : "string",
                  "minLength" : 3,
                  "maxLength" : 3
                }
              }
            } ]
          },
          "MiscFieldSettings" : {
            "type" : "object",
            "required" : [ "agent", "generalNumber", "insuranceObjectPrice", "insurerAccount", "number", "paymentDate", "paymentNumber" ],
            "properties" : {
              "number" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "generalNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "agent" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "insurerAccount" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "paymentDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "paymentNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "insuranceObjectPrice" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "Person" : {
            "type" : "object",
            "required" : [ "dontHaveCode", "legal" ],
            "properties" : {
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10,
                "pattern" : "[0-9]*"
              },
              "dontHaveCode" : {
                "type" : "boolean",
                "default" : false
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "nameLast" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "nameFirst" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 75
              },
              "nameMiddle" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 75
              },
              "address" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "phone" : {
                "type" : "string",
                "minLength" : 12,
                "maxLength" : 13
              },
              "email" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "birthDate" : {
                "type" : "string",
                "format" : "date-time"
              },
              "document" : {
                "$ref" : "#/definitions/PersonDocument"
              },
              "legal" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "PersonDocument" : {
            "type" : "object",
            "properties" : {
              "type" : {
                "type" : "string",
                "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
              },
              "series" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 4
              },
              "number" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "date" : {
                "type" : "string",
                "format" : "date-time"
              },
              "issuedBy" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              }
            }
          },
          "Place" : {
            "type" : "object",
            "required" : [ "cdbMtibu", "country", "zone" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "placeCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "nameRus" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "nameFull" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "cdbMtibu" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "Policy" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "bonusMalus", "drivingExpLessThreeYears", "franchise", "k1", "k2", "k3", "k4", "k5", "k6", "privilege", "taxi", "usageMonths" ],
              "properties" : {
                "usageMonths" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "franchise" : {
                  "type" : "number"
                },
                "k1" : {
                  "type" : "number"
                },
                "k2" : {
                  "type" : "number"
                },
                "k3" : {
                  "type" : "number"
                },
                "k4" : {
                  "type" : "number"
                },
                "k5" : {
                  "type" : "number"
                },
                "k6" : {
                  "type" : "number"
                },
                "bonusMalus" : {
                  "type" : "number"
                },
                "privilege" : {
                  "type" : "number"
                },
                "privilegeType" : {
                  "type" : "string",
                  "enum" : [ "VETERAN", "DISABLED", "CHERNOBYLETS", "PENSIONER" ]
                },
                "drivingExpLessThreeYears" : {
                  "type" : "boolean",
                  "default" : false
                },
                "taxi" : {
                  "type" : "boolean",
                  "default" : false
                },
                "stickerNumber" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 10
                },
                "requestUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "policyUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                }
              },
              "description" : "Полис ОСАГО"
            } ]
          },
          "PolicyFieldSettings" : {
            "type" : "object",
            "required" : [ "bonusMalus", "drivingExp", "k2", "k3", "k4", "sticker", "usageMonths" ],
            "properties" : {
              "usageMonths" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "drivingExp" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k2" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k3" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k4" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "bonusMalus" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "sticker" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "PolicyRu" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "bonusMalus", "category", "driversCount", "enginePower", "taxi" ],
              "properties" : {
                "category" : {
                  "type" : "string",
                  "description" : "Категория авто",
                  "enum" : [ "A", "B", "C1", "C2", "D1", "D2", "E", "F", "G" ]
                },
                "enginePower" : {
                  "type" : "string",
                  "description" : "Мощность двигателя",
                  "enum" : [ "LESS_50", "BETWEEN_50_70", "BETWEEN_70_100", "BETWEEN_100_120", "BETWEEN_120_150", "GREATHER_150" ]
                },
                "minSeniorityAndAge" : {
                  "type" : "string",
                  "enum" : [ "LESS_22_LESS_3", "GREATHER_22_LESS_3", "LESS_22_GREATHER_3", "GREATHER_22_GREATHER_3" ]
                },
                "driversCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "bonusMalus" : {
                  "type" : "number"
                },
                "taxi" : {
                  "type" : "boolean",
                  "default" : false
                },
                "driver1" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver2" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver3" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver4" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver5" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "kTerritory" : {
                  "type" : "number"
                },
                "kEngine" : {
                  "type" : "number"
                },
                "kSeniority" : {
                  "type" : "number"
                },
                "kViolations" : {
                  "type" : "number"
                }
              },
              "description" : "Базовый класс договора"
            } ]
          },
          "PolicyRuDriver" : {
            "type" : "object",
            "properties" : {
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "license" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              }
            }
          },
          "PolicyRuFieldSettings" : {
            "type" : "object",
            "required" : [ "bonusMalus" ],
            "properties" : {
              "bonusMalus" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "PropertyFieldSettings" : {
            "type" : "object",
            "required" : [ "propertyAddress", "propertyDescription", "propertyFloor", "propertyLivingSpace", "propertyNumberOfFloors", "propertyOwner", "propertyPlace", "propertyTotalSpace" ],
            "properties" : {
              "propertyPlace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyAddress" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyTotalSpace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyLivingSpace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyFloor" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyNumberOfFloors" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyOwner" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyDescription" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "ReportTemplate" : {
            "type" : "object",
            "required" : [ "dateFrom", "name", "templateType" ],
            "discriminator" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "notes" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 1024
              },
              "hasDraft" : {
                "type" : "boolean",
                "default" : false
              },
              "parameters" : {
                "type" : "string"
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "templateType" : {
                "type" : "string",
                "enum" : [ "JASPER", "HTML" ]
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "ReportTemplateWithContent" : {
            "allOf" : [ {
              "$ref" : "#/definitions/ReportTemplate"
            }, {
              "type" : "object",
              "properties" : {
                "content" : {
                  "type" : "string"
                }
              }
            } ]
          },
          "Risk" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор риска"
              },
              "shortName" : {
                "type" : "string",
                "description" : "Краткое название риска",
                "minLength" : 0,
                "maxLength" : 50
              },
              "name" : {
                "type" : "string",
                "description" : "Название риска",
                "minLength" : 0,
                "maxLength" : 100
              },
              "required" : {
                "type" : "boolean",
                "description" : "Риск обязателен для использования в страховом продукте",
                "default" : false
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32",
                "description" : "Позиция отображения"
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор риска во внешней системе"
              }
            },
            "description" : "Риски"
          },
          "SalePointSimple" : {
            "type" : "object",
            "required" : [ "company" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "идентификатор точки продаж"
              },
              "company" : {
                "description" : "компания, которому принадлежит точка продаж",
                "$ref" : "#/definitions/CompanySimple"
              },
              "code" : {
                "type" : "string",
                "description" : "код точки продаж",
                "minLength" : 0,
                "maxLength" : 100
              },
              "name" : {
                "type" : "string",
                "description" : "наименование точки продаж",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "description" : "наименование для печати",
                "minLength" : 0,
                "maxLength" : 120
              },
              "lft" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "левое значение NestedSet"
              },
              "rgt" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "правое значение NestedSet"
              },
              "path" : {
                "type" : "string",
                "description" : "путь в дереве (id через запятую)"
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffCustom" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffSimple"
            }, {
              "type" : "object",
              "required" : [ "blankUsage", "calcType", "contractDateLimitType", "contractType", "customerCategory", "dateFromLimitType", "insuranceObjectType", "maxObjectCount", "objectsCalcType" ],
              "properties" : {
                "customerCategory" : {
                  "type" : "string",
                  "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
                },
                "reportTemplates" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ReportTemplate"
                  }
                },
                "customFields" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomField"
                  }
                },
                "contractDateLimitType" : {
                  "type" : "string",
                  "enum" : [ "TODAY", "BEFORE_TODAY" ]
                },
                "contractDateLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "dateFromLimitType" : {
                  "type" : "string",
                  "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
                },
                "dateFromLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "freeDateTo" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "periods" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPeriod"
                  }
                },
                "roundPayment" : {
                  "type" : "boolean",
                  "default" : false
                },
                "actionDescription" : {
                  "type" : "string"
                },
                "discountDescription" : {
                  "type" : "string"
                },
                "blankUsage" : {
                  "type" : "string",
                  "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
                },
                "blankAutoSelect" : {
                  "type" : "boolean",
                  "default" : false
                },
                "blankSearchUp" : {
                  "type" : "boolean",
                  "default" : false
                },
                "hasBeneficiary" : {
                  "type" : "boolean",
                  "default" : false
                },
                "proportionalUrgency" : {
                  "type" : "boolean",
                  "default" : false
                },
                "urgencyValues" : {
                  "type" : "array",
                  "items" : {
                    "type" : "number"
                  }
                },
                "state" : {
                  "type" : "string",
                  "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
                },
                "hasPartnerTariff" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dateFromWithTime" : {
                  "type" : "boolean",
                  "default" : false
                },
                "insuranceObjectType" : {
                  "type" : "string",
                  "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
                },
                "maxObjectCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "objectsCalcType" : {
                  "type" : "string",
                  "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
                },
                "customerSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "beneficiarySettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "miscSettings" : {
                  "$ref" : "#/definitions/MiscFieldSettings"
                },
                "autoSettings" : {
                  "$ref" : "#/definitions/AutoFieldSettings"
                },
                "propertySettings" : {
                  "$ref" : "#/definitions/PropertyFieldSettings"
                },
                "personSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "paymentOptions" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calculationKinds" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calcType" : {
                  "type" : "string",
                  "enum" : [ "CALC_PAYMENT", "CALC_AMOUNT", "CALC_TARIFF" ]
                },
                "contractType" : {
                  "$ref" : "#/definitions/ContractType"
                },
                "baseTariff" : {
                  "type" : "number"
                },
                "freeInsuranceAmount" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minInsuranceAmount" : {
                  "type" : "number"
                },
                "maxInsuranceAmount" : {
                  "type" : "number"
                },
                "defaultInsuranceAmount" : {
                  "type" : "number"
                },
                "insuranceAmounts" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomAmount"
                  }
                },
                "commission" : {
                  "type" : "number",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                },
                "brokerDiscount" : {
                  "type" : "number",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                }
              },
              "description" : "Базовый класс тарифов"
            } ]
          },
          "TariffCustomAmount" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "amount" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              },
              "description" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              }
            }
          },
          "TariffCustomField" : {
            "type" : "object",
            "required" : [ "calculationKind", "contractBlock", "customField", "fieldVisibility" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "customField" : {
                "$ref" : "#/definitions/CustomField"
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32"
              },
              "fieldVisibility" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "contractBlock" : {
                "type" : "string",
                "enum" : [ "CALCULATION", "CUSTOMER", "BENEFICIARY", "INSURANCE_OBJECT", "OTHER" ]
              },
              "calculationKind" : {
                "type" : "string",
                "enum" : [ "NONE", "TARIFF_MULTIPLIER", "TARIFF_ADDITION", "PAYMENT_MULTIPLIER", "PAYMENT_ADDITION" ]
              },
              "members" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomFieldListMember"
                }
              }
            }
          },
          "TariffCustomFieldListMember" : {
            "type" : "object",
            "required" : [ "listMember" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "listMember" : {
                "$ref" : "#/definitions/CustomListFieldMember"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              },
              "defaultMember" : {
                "type" : "boolean",
                "default" : false
              },
              "coeff" : {
                "type" : "number"
              }
            }
          },
          "TariffGreenCard" : {
            "type" : "object",
            "required" : [ "blankUsage", "company", "contractDateLimitType", "customerCategory", "dateFrom", "dateFromLimitType", "insuranceObjectType", "insurer", "maxObjectCount", "objectsCalcType" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "commissionForEurope" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "commissionForBMR" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "maxDiscount" : {
                "type" : "number"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Базовый класс тарифов"
          },
          "TariffPeriod" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "period" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              }
            }
          },
          "TariffPolicy" : {
            "type" : "object",
            "required" : [ "blankUsage", "company", "contractDateLimitType", "customerCategory", "dateFrom", "dateFromLimitType", "franchise", "insuranceObjectType", "insurer", "maxObjectCount", "medium", "minBonusMalus", "objectsCalcType", "policySettings" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "periods" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPeriod"
                }
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "franchise" : {
                "type" : "number"
              },
              "taxi" : {
                "type" : "boolean",
                "default" : false
              },
              "minBonusMalus" : {
                "type" : "number"
              },
              "medium" : {
                "type" : "string",
                "enum" : [ "PAPER", "ELECTRONIC" ]
              },
              "k2Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK2"
                }
              },
              "k3Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK3"
                }
              },
              "k4Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK4"
                }
              },
              "maxDiscount" : {
                "type" : "number"
              },
              "commission" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "brokerDiscount" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "useCdbMtibu" : {
                "type" : "boolean",
                "default" : false
              },
              "policySettings" : {
                "$ref" : "#/definitions/PolicyFieldSettings"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Тариф для ОСАГО"
          },
          "TariffPolicyChoose" : {
            "type" : "object",
            "properties" : {
              "tariff" : {
                "readOnly" : true,
                "$ref" : "#/definitions/TariffPolicy"
              },
              "payment" : {
                "type" : "number",
                "readOnly" : true
              },
              "commission" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedPayment" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedCommission" : {
                "type" : "number",
                "readOnly" : true
              },
              "approx" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "canCreated" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "actionDescription" : {
                "type" : "string",
                "readOnly" : true
              },
              "discountDescription" : {
                "type" : "string",
                "readOnly" : true
              }
            }
          },
          "TariffPolicyK2" : {
            "type" : "object",
            "required" : [ "zone" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffPolicyK3" : {
            "type" : "object",
            "required" : [ "autoCategory" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffPolicyK4" : {
            "type" : "object",
            "required" : [ "maxValueAbove3", "maxValueLess3", "minValueAbove3", "minValueLess3" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "minValueLess3" : {
                "type" : "number"
              },
              "maxValueLess3" : {
                "type" : "number"
              },
              "minValueAbove3" : {
                "type" : "number"
              },
              "maxValueAbove3" : {
                "type" : "number"
              }
            }
          },
          "TariffPolicyRu" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffSimple"
            }, {
              "type" : "object",
              "required" : [ "blankUsage", "contractDateLimitType", "customerCategory", "dateFromLimitType", "insuranceObjectType", "maxObjectCount", "minBonusMalus", "objectsCalcType", "policyRuFieldSettings" ],
              "properties" : {
                "customerCategory" : {
                  "type" : "string",
                  "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
                },
                "reportTemplates" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ReportTemplate"
                  }
                },
                "customFields" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomField"
                  }
                },
                "contractDateLimitType" : {
                  "type" : "string",
                  "enum" : [ "TODAY", "BEFORE_TODAY" ]
                },
                "contractDateLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "dateFromLimitType" : {
                  "type" : "string",
                  "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
                },
                "dateFromLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "freeDateTo" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "periods" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPeriod"
                  }
                },
                "roundPayment" : {
                  "type" : "boolean",
                  "default" : false
                },
                "actionDescription" : {
                  "type" : "string"
                },
                "discountDescription" : {
                  "type" : "string"
                },
                "blankUsage" : {
                  "type" : "string",
                  "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
                },
                "blankAutoSelect" : {
                  "type" : "boolean",
                  "default" : false
                },
                "blankSearchUp" : {
                  "type" : "boolean",
                  "default" : false
                },
                "hasBeneficiary" : {
                  "type" : "boolean",
                  "default" : false
                },
                "proportionalUrgency" : {
                  "type" : "boolean",
                  "default" : false
                },
                "urgencyValues" : {
                  "type" : "array",
                  "items" : {
                    "type" : "number"
                  }
                },
                "state" : {
                  "type" : "string",
                  "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
                },
                "hasPartnerTariff" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dateFromWithTime" : {
                  "type" : "boolean",
                  "default" : false
                },
                "insuranceObjectType" : {
                  "type" : "string",
                  "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
                },
                "maxObjectCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "objectsCalcType" : {
                  "type" : "string",
                  "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
                },
                "customerSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "beneficiarySettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "miscSettings" : {
                  "$ref" : "#/definitions/MiscFieldSettings"
                },
                "autoSettings" : {
                  "$ref" : "#/definitions/AutoFieldSettings"
                },
                "propertySettings" : {
                  "$ref" : "#/definitions/PropertyFieldSettings"
                },
                "personSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "paymentOptions" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calculationKinds" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "taxi" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minBonusMalus" : {
                  "type" : "number"
                },
                "baseTariffs" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPolicyRuBT"
                  }
                },
                "policyRuFieldSettings" : {
                  "$ref" : "#/definitions/PolicyRuFieldSettings"
                }
              },
              "description" : "Базовый класс тарифов"
            } ]
          },
          "TariffPolicyRuBT" : {
            "type" : "object",
            "required" : [ "autoCategory" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A", "B", "C1", "C2", "D1", "D2", "E", "F", "G" ]
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffSimple" : {
            "type" : "object",
            "required" : [ "company", "dateFrom", "insurer" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Минимизированная информация по тарифу"
          },
          "TariffVcl" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffSimple"
            }, {
              "type" : "object",
              "required" : [ "blankUsage", "contractDateLimitType", "customerCategory", "dateFromLimitType", "insuranceObjectType", "maxObjectCount", "objectsCalcType" ],
              "properties" : {
                "customerCategory" : {
                  "type" : "string",
                  "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
                },
                "reportTemplates" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ReportTemplate"
                  }
                },
                "customFields" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomField"
                  }
                },
                "contractDateLimitType" : {
                  "type" : "string",
                  "enum" : [ "TODAY", "BEFORE_TODAY" ]
                },
                "contractDateLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "dateFromLimitType" : {
                  "type" : "string",
                  "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
                },
                "dateFromLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "freeDateTo" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "periods" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPeriod"
                  }
                },
                "roundPayment" : {
                  "type" : "boolean",
                  "default" : false
                },
                "actionDescription" : {
                  "type" : "string"
                },
                "discountDescription" : {
                  "type" : "string"
                },
                "blankUsage" : {
                  "type" : "string",
                  "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
                },
                "blankAutoSelect" : {
                  "type" : "boolean",
                  "default" : false
                },
                "blankSearchUp" : {
                  "type" : "boolean",
                  "default" : false
                },
                "hasBeneficiary" : {
                  "type" : "boolean",
                  "default" : false
                },
                "proportionalUrgency" : {
                  "type" : "boolean",
                  "default" : false
                },
                "urgencyValues" : {
                  "type" : "array",
                  "items" : {
                    "type" : "number"
                  }
                },
                "state" : {
                  "type" : "string",
                  "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
                },
                "hasPartnerTariff" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dateFromWithTime" : {
                  "type" : "boolean",
                  "default" : false
                },
                "insuranceObjectType" : {
                  "type" : "string",
                  "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
                },
                "maxObjectCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "objectsCalcType" : {
                  "type" : "string",
                  "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
                },
                "customerSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "beneficiarySettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "miscSettings" : {
                  "$ref" : "#/definitions/MiscFieldSettings"
                },
                "autoSettings" : {
                  "$ref" : "#/definitions/AutoFieldSettings"
                },
                "propertySettings" : {
                  "$ref" : "#/definitions/PropertyFieldSettings"
                },
                "personSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "paymentOptions" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calculationKinds" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "commission" : {
                  "type" : "number",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                },
                "brokerDiscount" : {
                  "type" : "number",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                },
                "limits" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffVclLimit"
                  }
                }
              },
              "description" : "Базовый класс тарифов"
            } ]
          },
          "TariffVclLimit" : {
            "type" : "object",
            "required" : [ "limit", "payment" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              },
              "limit" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              }
            }
          },
          "User" : {
            "type" : "object",
            "required" : [ "dateFrom", "salePoint" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "lastName" : {
                "type" : "string",
                "description" : "Фамилия пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "firstName" : {
                "type" : "string",
                "description" : "Имя пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "middleName" : {
                "type" : "string",
                "description" : "Отчество пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "email" : {
                "type" : "string",
                "description" : "Электронная почта пользователя"
              },
              "code" : {
                "type" : "string",
                "description" : "ИНН пользователя",
                "minLength" : 10,
                "maxLength" : 10
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор пользователя во внешней системе системе (в системе компании)"
              },
              "salePoint" : {
                "description" : "точка продаж которой принадлежит пользователь",
                "$ref" : "#/definitions/SalePointSimple"
              },
              "password" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 40
              },
              "phone" : {
                "type" : "string",
                "minLength" : 12,
                "maxLength" : 13
              },
              "permissions" : {
                "$ref" : "#/definitions/UserPermissions"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "settings" : {
                "$ref" : "#/definitions/UserSettings"
              }
            },
            "description" : "Пользователь системы"
          },
          "UserPermissions" : {
            "type" : "object",
            "required" : [ "canEditElectronicPolicy", "canEditPolicy", "canEditSalePoints", "canEditTariffs", "canEditUsers", "canEditWiki", "canSearchCustomers", "canViewPolicy", "companyAdmin", "superAdmin" ],
            "properties" : {
              "canEditPolicy" : {
                "type" : "string",
                "enum" : [ "NONE", "CREATE", "SIGN", "UNSIGN", "REQUEST", "EMIT" ]
              },
              "canEditElectronicPolicy" : {
                "type" : "string",
                "enum" : [ "NONE", "CREATE", "SIGN", "UNSIGN", "REQUEST", "EMIT" ]
              },
              "canViewPolicy" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canEditBlanks" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canSearchCustomers" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canEditSalePoints" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditUsers" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditTariffs" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditWiki" : {
                "type" : "boolean",
                "default" : false
              },
              "superAdmin" : {
                "type" : "boolean",
                "default" : false
              },
              "companyAdmin" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "UserSettings" : {
            "type" : "object",
            "properties" : {
              "printOffsetX" : {
                "type" : "integer",
                "format" : "int32"
              },
              "printOffsetY" : {
                "type" : "integer",
                "format" : "int32"
              }
            }
          },
          "UserSimple" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "lastName" : {
                "type" : "string",
                "description" : "Фамилия пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "firstName" : {
                "type" : "string",
                "description" : "Имя пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "middleName" : {
                "type" : "string",
                "description" : "Отчество пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "email" : {
                "type" : "string",
                "description" : "Электронная почта пользователя"
              },
              "code" : {
                "type" : "string",
                "description" : "ИНН пользователя",
                "minLength" : 10,
                "maxLength" : 10
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор пользователя во внешней системе системе (в системе компании)"
              }
            },
            "description" : "Пользователь системы (упрощенный)"
          },
          "Vcl" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "limit" ],
              "properties" : {
                "limit" : {
                  "type" : "number"
                }
              },
              "description" : "Базовый класс договора"
            } ]
          }
        }
      },
      "custom": {
        articles: {
          "default": {
            title: "Общие положения",
            body: {
              "description": {
                type: "ElementDescription",
                text: '<p>Регистрация собственного адреса<br>Комнании company.ua (адрес взят для примера) для регистрации собственного адреса EWA необходимо в DNS-настройках домена company.ua добавить CNAME-запись ewa.company.ua со ссылкой на хост web.ewa.ua<br>При входе в систему по собственному адресу на форме логина будет отображаться персональный логотип компании.</p><p>Для выполнения тестовых запросов необходимо обращаться к тестовому серверу test.ewa.ua. Для выполнения продакшн-запросов необходимо обращаться к собственному проброшенному адресу (ewa.company.ua из описания выше) либо web.ewa.ua, если собственный адрес не настроен.</p><p>Соединение с web.ewa.ua защищено и шифруется при помощи SSL-сертификата.<br>Для защиты собственного адреса компании необходим SSL-сертификат для адреса ewa.company.ua. Если у компании уже есть SSL-сертификат с защитой субдоменов (Wildcard) *.company.ua, то дополнительный сертификат не нужен. Если имеющийся сертификат не распространяется на адрес ewa.company.ua, то достаточно приобрести SSL-сертификат непосредственно на адрес ewa.company.ua сроком от 1 года (к примеру, на ssl.com.ua), либо сгенерировать бесплатный сертификат на letsencrypt.org (сейчас максимальный срок действия сертификата 3 месяца, в перспективе может быть уменьшен до 2 недель).</p>'
              }
            }
          },
          "api-active_services-general": {
            title: "Общие положения",
            body: {
              "description": {
                type: "ElementDescription",
                text:
                  '<p align="justify">Активные веб-сервисы срабатывают при неких событиях в EWA (например, при сохранении договора) либо по расписанию. При наступлении события формируется REST-запрос и отправляется на сервер клиенту.<p>' +
                  '<h2>Процедура настройки обмена данными между системой EWA и внешней системой клиента</h2>' +
                  '<ul>' +
                  '  <li>Клиент со своей стороны реализует только необходимые ему сервисы.</li>' +
                  '  <li>Все активные WEB-сервисы независимы. Каждый сервис настраивается отдельно.</li>' +
                  '  <li>Для каждого WEB-сервиса клиент выбирает необходимый ему метод REST-запроса(GET, POST и т.д.).</li>' +
                  '  <li>При необходимости в запрос можно добавить любое количество параметров-констант. Параметры могут быть установлены как в заголовок так и тело.</li>' +
                  '  <li align="justify">В зависимости от типа события, которое произошло в EWA, в запрос могут быть добавлены служебные параметры. Список служебных параметров описан для каждого сервиса отдельно. Клиент сам решает, какие служебные параметры нужно добавить.</b></li>' +
                  '</ul>' +
                  '<h2>ВАЖНО</h2>' +
                  '<ul>' +
                  '  <li>Формат ответа должен полностью отвечать требованиям EWA. Иначе запрос будет считаться невыполненным.</li>' +
                  '  <li>Тип и формат (например, для даты) служебных параметров изменить нельзя.</li>' +
                  '  <li>В описании служебных параметров указаны имена по умолчанию. По желанию клиента, имена могут быть изменены.</li>' +
                  '</ul>'
              }
            }
          },
          "api-active_services-auth": {
            title: "Авторизация на сервере клиента"
          },
          "api-active_services-person": {
            title: "Получение списка контрагентов по ИНН",
            body: {
              "description": {
                type: "ElementDescription",
                order: 0,
                text: 'Сервис используется для получения списка контрагентов, идентификационный номер которых начинается с [query].'
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "isLegal": {
                    description: "Признак. Юридическое лицо",
                    order: 0,
                    paramType: {
                      name: "boolean"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 1,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  },
                  "documentTypes": {
                    description: "Список допустимых типов для документа, удостоверяюещего личность. Разделитель списка - запятая",
                    order: 2,
                    paramType: {
                      name: "string",
                      allowedValues: {
                        "PASSPORT": {},
                        "DRIVING_LICENSE": {},
                        "RESIDENCE_PERMIT": {},
                        "VETERAN_CERTIFICATE": {},
                        "REGISTRATION_CARD": {},
                        "EXTERNAL_PASSPORT": {},
                        "FOREIGN_PASSPORT": {},
                        "PENSION_CERTIFICATE": {},
                        "DISABILITY_CERTIFICATE": {},
                        "CHERNOBYL_CERTIFICATE": {}
                      }
                    }
                  },
                  "query": {
                    description: "Идентификационный номер контрагента",
                    order: 3,
                    paramType: {
                      name: "string"
                    }
                  },
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных контрагентов",
                    paramType: {
                      name: "array of ",
                      ref: "model-Person",
                      refName: "Person"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-auto-vin": {
            title: "Поиск транспортного средства по VIN-коду",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка транспортных средств, VIN-код которых начинается с [vin].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "vin": {
                    description: "Начальные символы VIN-кода",
                    paramType: {
                      name: "string"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных транспортных средств",
                    paramType: {
                      name: "array of ",
                      ref: "model-InsuranceObjectAuto",
                      refName: "InsuranceObjectAuto"
                    }
                  }
                }
              }
            },
          },
          "api-active_services-auto-state_number": {
            title: "Поиск транспортного средства по государственному номеру",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка транспортных средств, государственный номер которых начинается с [stateNumber].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "stateNumber": {
                    description: "Начальные символы государственного номера",
                    paramType: {
                      name: "string"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных транспортных средств",
                    paramType: {
                      name: "array of ",
                      ref: "model-InsuranceObjectAuto",
                      refName: "InsuranceObjectAuto"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-blank": {
            title: "Получение списка бланков по номеру и типу на дату",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка бланков, номера которых начинаются с [query].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "query": {
                    description: "Первые несколько символов номера",
                    order: 0,
                    paramType: {
                      name: "string"
                    }
                  },
                  "blankType": {
                    description: "Тип бланка",
                    order: 1,
                    paramType: {
                      name: "string",
                    }
                  },
                  "blankSearchUp": {
                    description: "Признак. Искать бланк начиная с текущей точки продаж и вверх по дереву агентской сети",
                    order: 2,
                    paramType: {
                      name: "boolean"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 3,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных номеров",
                    paramType: {
                      name: "array of ",
                      refName: "string"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-agent": {
            title: "Получение списка агентов по коду на дату",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка агентов, номер которых начинается с [query].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "query": {
                    description: "Первые несколько символов кода",
                    order: 0,
                    paramType: {
                      name: "string"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 1,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных кодов агентов",
                    paramType: {
                      name: "array of ",
                      refName: "string"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-export": {
            title: "Экспорт данных о реализованном договоре",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис экспортирует данные о реализованном договоре при любом его изменении. <br><b>ВНИМАНИЕ! Договор не будет выгружен, если закрыт период вызгрузки в настройках компании.</b>',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "contract": {
                    description: "Данные договора",
                    paramType: {
                      ref: "model-Contract",
                      refName: "Contract"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Информация о состоянии выгрузки",
                    paramType: {
                      ref: "model-ExportResult",
                      refName: "ExportResult"
                    }
                  }
                }
              }
            }
          },
        },
        navigation: {
          sections: {
            "general": {
              order: 0,
              title: "Общая информация",
              tags: {
                "general": {
                  articles: {
                    "default": {
                      title: "Общие положения",
                      order: 0
                    },
                  }
                }
              }
            },
            "active_services": {
              order: 1,
              title: "Активные сервисы",
              tags: {
                "api-active_services": {
                  order: 0,
                  articles: {
                    "api-active_services-general": {
                      order: 0,
                      title: "Общие положения"
                    },
                    "api-active_services-auth": {
                      order: 1,
                      title: "Авторизация на сервере клиента"
                    },
                    "api-active_services-person": {
                      order: 2,
                      title: "Поиск контрагента по ИНН"
                    },
                    "api-active_services-blank": {
                      order: 3,
                      title: "Поиск бланка, стикера"
                    },
                    "api-active_services-agent": {
                      order: 4,
                      title: "Поиск агента"
                    },
                    "api-active_services-auto-vin": {
                      order: 5,
                      title: "Поиск ТС по VIN"
                    },
                    "api-active_services-auto-state_number": {
                      order: 6,
                      title: "Поиск ТС по гос. номеру"
                    },
                    "api-active_services-export": {
                      order: 7,
                      title: "Экспорт договора"
                    }
                  }
                }
              }
            }
          }
        }
      },
    },
    "2016.06-SNAPSHOT": {
      "swagger": {
        "swagger" : "2.0",
        "info" : {
          "version" : "2016.06-SNAPSHOT",
          "title" : "EWA.REST-API.DOCS"
        },
        "host" : "test.ewa.ua",
        "basePath" : "/evo/api",
        "tags" : [ {
          "name" : "Договоры"
        }, {
          "name" : "Пользователи"
        }, {
          "name" : "Справочники"
        }, {
          "name" : "Тарифы"
        } ],
        "schemes" : [ "https" ],
        "paths" : {
          "/auto_model/maker_and_model" : {
            "get" : {
              "tags" : [ "Справочники" ],
              "summary" : "Поиск марки и модели авто",
              "description" : "",
              "operationId" : "list",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Строка, по которой будет производиться поиск",
                "required" : false,
                "type" : "string"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "type" : "object"
                    }
                  }
                }
              }
            }
          },
          "/contract/save" : {
            "post" : {
              "tags" : [ "Договоры" ],
              "summary" : "Сохранение договора",
              "description" : "Доступно для CAN_EDIT_CONTRACT<br/>Возвращает сохраненный договор.",
              "operationId" : "save",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "in" : "body",
                "name" : "body",
                "description" : "Сохраняемый договор",
                "required" : false,
                "schema" : {
                  "$ref" : "#/definitions/Contract"
                }
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/Contract"
                  }
                }
              }
            }
          },
          "/place" : {
            "get" : {
              "tags" : [ "Справочники" ],
              "summary" : "Поиск населённого пункта",
              "description" : "",
              "operationId" : "list",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Строка, по которой будет производиться поиск",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "country",
                "in" : "query",
                "description" : "Аббревиатура страны для поиска населенного пункта. Например, UA",
                "required" : false,
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              }, {
                "name" : "cdbMtibu",
                "in" : "query",
                "description" : "Признак. Производить поиск только в справочнике населенных пунктов МТСБУ",
                "required" : false,
                "type" : "boolean"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "type" : "object"
                    }
                  }
                }
              }
            }
          },
          "/tariff" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Список тарифов",
              "description" : "",
              "operationId" : "list",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "onlyActive",
                "in" : "query",
                "description" : "Признак. Искать только активные тарифы для текущего пользователя",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "companyId",
                "in" : "query",
                "description" : "Идентификатор компании, которой принадлежит тариф",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "Идентификатор точки продаж, для которой отбираются тарифы",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "showChildSalePointTariffs",
                "in" : "query",
                "description" : "Признак. Отображать тарифы подчиненных точек продаж",
                "required" : false,
                "type" : "boolean"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "type" : "object"
                    }
                  }
                }
              }
            }
          },
          "/tariff/choose/greencard" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Калькулятор \"Зеленая карта\"",
              "description" : "Подбор тарифа для договоров \"Зеленой карты\"",
              "operationId" : "chooseGreenCardTariff",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "salePoint",
                "in" : "query",
                "description" : "Идентификатор точки продаж для которой (и ее подчиненных) отбираются тарифы. Если не задан, то берется идентификатор точки пользователя",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "dateFrom",
                "in" : "query",
                "description" : "Начальная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "area",
                "in" : "query",
                "description" : "Территория покрытия будущего договора",
                "required" : false,
                "type" : "string",
                "enum" : [ "BMR", "EUROPE" ]
              }, {
                "name" : "type",
                "in" : "query",
                "description" : "Тип транспортного средства",
                "required" : false,
                "type" : "string",
                "enum" : [ "A", "B", "C", "E", "F" ]
              }, {
                "name" : "period",
                "in" : "query",
                "description" : "Период действия договора",
                "required" : false,
                "type" : "string",
                "enum" : [ "FIFTEEN_DAYS", "ONE_MONTH", "TWO_MONTHS", "THREE_MONTHS", "FOUR_MONTHS", "FIVE_MONTHS", "SIX_MONTHS", "SEVEN_MONTHS", "EIGHT_MONTHS", "NINE_MONTHS", "TEN_MONTHS", "ELEVEN_MONTHS", "YEAR" ]
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/GreenCardTariffChoose"
                    }
                  }
                }
              }
            }
          },
          "/tariff/choose/policy" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Калькулятор \"ОСАГО\"",
              "description" : "Подбор тарифа для договоров \"ОСАГО\"",
              "operationId" : "choosePolicyTariff",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "salePoint",
                "in" : "query",
                "description" : "Идентификатор точки продаж для которой (и ее подчиненных) отбираются тарифы.Если не задан, то берется идентификатор точки пользователя",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "customerCategory",
                "in" : "query",
                "description" : "Категория контрагента",
                "required" : false,
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              }, {
                "name" : "franchise",
                "in" : "query",
                "description" : "Франшиза",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "bonusMalus",
                "in" : "query",
                "description" : "Бонус-малус",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "discount",
                "in" : "query",
                "description" : "Скидка",
                "required" : false,
                "type" : "number"
              }, {
                "name" : "taxi",
                "in" : "query",
                "description" : "Признак. Авто используется как такси",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "autoCategory",
                "in" : "query",
                "description" : "Категория авто",
                "required" : false,
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              }, {
                "name" : "zone",
                "in" : "query",
                "description" : "Зона регистрации авто",
                "required" : false,
                "type" : "integer",
                "format" : "int32"
              }, {
                "name" : "dateFrom",
                "in" : "query",
                "description" : "Начальная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "dateTo",
                "in" : "query",
                "description" : "Конечная дата будущего договора",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "usageMonths",
                "in" : "query",
                "description" : "Месяцы неиспользования",
                "required" : false,
                "type" : "integer",
                "format" : "int32"
              }, {
                "name" : "driveExp",
                "in" : "query",
                "description" : "Признак. Стаж вождения менее трех лет",
                "required" : false,
                "type" : "boolean"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffPolicyChoose"
                    }
                  }
                }
              }
            }
          },
          "/tariff/custom" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Список коробочных тарифов",
              "description" : "",
              "operationId" : "listOfCustomTariffs",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Часть названия для поиска",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "onlyActive",
                "in" : "query",
                "description" : "Признак. Искать только активные тарифы для текущего пользователя",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "contractType",
                "in" : "query",
                "description" : "Код типа коробочного договора",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "companyId",
                "in" : "query",
                "description" : "Идентификатор компании, которой принадлежит тариф",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "Идентификатор точки продаж, для которой отбираются тарифы",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffCustom"
                    }
                  }
                }
              }
            }
          },
          "/tariff/greencard" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Список тарифов \"Зеленая карта\"",
              "description" : "",
              "operationId" : "listOfGreenCardTariffs",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Часть названия для поиска",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "onlyActive",
                "in" : "query",
                "description" : "Признак. Искать только активные тарифы для текущего пользователя",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "companyId",
                "in" : "query",
                "description" : "Идентификатор компании, которой принадлежит тариф",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "Идентификатор точки продаж, для которой отбираются тарифы",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffGreenCard"
                    }
                  }
                }
              }
            }
          },
          "/tariff/policy" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Список тарифов \"ОСАГО\"",
              "description" : "",
              "operationId" : "listOfPolicyTariffs",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Часть названия для поиска",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "onlyActive",
                "in" : "query",
                "description" : "Признак. Искать только активные тарифы для текущего пользователя",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "companyId",
                "in" : "query",
                "description" : "Идентификатор компании, которой принадлежит тариф",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "Идентификатор точки продаж, для которой отбираются тарифы",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffCustom"
                    }
                  }
                }
              }
            }
          },
          "/tariff/vcl" : {
            "get" : {
              "tags" : [ "Тарифы" ],
              "summary" : "Список тарифов \"ДГО\"",
              "description" : "",
              "operationId" : "listOfVCLTariffs",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "query",
                "in" : "query",
                "description" : "Часть названия для поиска",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "onlyActive",
                "in" : "query",
                "description" : "Признак. Искать только активные тарифы для текущего пользователя",
                "required" : false,
                "type" : "boolean"
              }, {
                "name" : "companyId",
                "in" : "query",
                "description" : "Идентификатор компании, которой принадлежит тариф",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              }, {
                "name" : "salePointId",
                "in" : "query",
                "description" : "Идентификатор точки продаж, для которой отбираются тарифы",
                "required" : false,
                "type" : "integer",
                "format" : "int64"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/definitions/TariffVcl"
                    }
                  }
                }
              }
            }
          },
          "/user/login" : {
            "post" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Вход в систему",
              "description" : "",
              "operationId" : "login",
              "produces" : [ "application/json" ],
              "parameters" : [ {
                "name" : "email",
                "in" : "formData",
                "description" : "email пользователя",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "password",
                "in" : "formData",
                "description" : "SHA1-дайджест пароля пользователя",
                "required" : false,
                "type" : "string"
              }, {
                "name" : "asuser",
                "in" : "formData",
                "description" : "Войти как. ТОЛЬКО для суперпользователей",
                "required" : false,
                "type" : "string"
              } ],
              "responses" : {
                "200" : {
                  "description" : "successful operation",
                  "schema" : {
                    "$ref" : "#/definitions/CurrentUserInfo"
                  }
                }
              }
            }
          },
          "/user/logout" : {
            "put" : {
              "tags" : [ "Пользователи" ],
              "summary" : "Выход из системы",
              "description" : "",
              "operationId" : "logout",
              "produces" : [ "application/json" ],
              "responses" : {
                "default" : {
                  "description" : "successful operation"
                }
              }
            }
          }
        },
        "definitions" : {
          "AutoFieldSettings" : {
            "type" : "object",
            "required" : [ "autoCategory", "autoColor", "autoEngineVolume", "autoModel", "autoOwner", "autoRegistrationDocument", "autoRegistrationPlace", "autoRegistrationType", "autoStateNumber", "autoVIN", "autoYear", "creditDocument", "pledgeDocument" ],
            "properties" : {
              "autoModel" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoVIN" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoStateNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationPlace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoYear" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoEngineVolume" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationType" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoRegistrationDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoColor" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "autoOwner" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "creditDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "pledgeDocument" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "AutoMaker" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентфикатор"
              },
              "name" : {
                "type" : "string",
                "description" : "Название модели авто",
                "minLength" : 0,
                "maxLength" : 20
              }
            },
            "description" : "Модель авто"
          },
          "AutoModel" : {
            "type" : "object",
            "required" : [ "autoMaker" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентфикатор"
              },
              "autoMaker" : {
                "description" : "Марка автомобиля",
                "$ref" : "#/definitions/AutoMaker"
              },
              "kind" : {
                "type" : "string",
                "description" : "Тип автомобиля",
                "enum" : [ "MOTO", "CAR", "PASSENGER", "FREIGHT", "TRAILER", "AGRICULTURAL", "SPECIAL" ]
              },
              "name" : {
                "type" : "string",
                "description" : "Название модели автомобиля",
                "minLength" : 0,
                "maxLength" : 50
              },
              "categories" : {
                "type" : "array",
                "items" : {
                  "type" : "string",
                  "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
                }
              }
            },
            "description" : "Модель атомобиля"
          },
          "Bank" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "mfo" : {
                "type" : "string"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              }
            }
          },
          "Broker" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CompanySimple"
            }, {
              "type" : "object",
              "required" : [ "dateFrom" ],
              "properties" : {
                "evoUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "smsName" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 11,
                  "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 100
                },
                "hotlinePhone" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "homePage" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "paymentOptions" : {
                  "type" : "string"
                },
                "calculationKinds" : {
                  "type" : "string"
                },
                "accounts" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyAccount"
                  }
                },
                "emails" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyEmail"
                  }
                },
                "contractTypes" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ContractType"
                  }
                },
                "dateFrom" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dateTo" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "hasDataHelper" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dataHelperParameters" : {
                  "type" : "string"
                },
                "closePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "exportClosePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                }
              }
            } ]
          },
          "Company" : {
            "type" : "object",
            "required" : [ "country", "dateFrom" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 8,
                "maxLength" : 10,
                "pattern" : "^[0-9]*$"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "exportersAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "evoUrl" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120,
                "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
              },
              "smsName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 11,
                "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
              },
              "address" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "hotlinePhone" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "homePage" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120,
                "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
              },
              "paymentOptions" : {
                "type" : "string"
              },
              "calculationKinds" : {
                "type" : "string"
              },
              "accounts" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CompanyAccount"
                }
              },
              "emails" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CompanyEmail"
                }
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "hasDataHelper" : {
                "type" : "boolean",
                "default" : false
              },
              "dataHelperParameters" : {
                "type" : "string"
              },
              "closePeriod" : {
                "type" : "string",
                "format" : "date-time"
              },
              "exportClosePeriod" : {
                "type" : "string",
                "format" : "date-time"
              }
            }
          },
          "CompanyAccount" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "mfo" : {
                "type" : "string"
              },
              "bank" : {
                "$ref" : "#/definitions/Bank"
              },
              "account" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 14
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              },
              "defaultAccount" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "CompanyEmail" : {
            "type" : "object",
            "required" : [ "email", "status" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "email" : {
                "type" : "string"
              },
              "status" : {
                "type" : "string",
                "enum" : [ "CONFIRMED", "NOT_CONFIRMED", "SENT" ]
              },
              "defaultEmail" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "CompanySimple" : {
            "type" : "object",
            "required" : [ "country" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 8,
                "maxLength" : 10,
                "pattern" : "^[0-9]*$"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 120
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "exportersAvailable" : {
                "type" : "boolean",
                "default" : false
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "Contract" : {
            "type" : "object",
            "required" : [ "beneficiary", "brokerDiscountedPayment", "customer", "date", "dateFrom", "dateTo", "discount", "multiObject", "payment", "salePoint", "state", "tariff", "urgencyCoefficient", "user" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор договора в системе EWA"
              },
              "code" : {
                "type" : "string",
                "description" : "Случайный буквенно-символьный код договора"
              },
              "salePoint" : {
                "description" : "Точка продаж, на которой был реализован договор",
                "$ref" : "#/definitions/SalePointSimple"
              },
              "user" : {
                "description" : "Пользователь, который внес договор",
                "$ref" : "#/definitions/UserSimple"
              },
              "created" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "Дата создания договора"
              },
              "stateChanged" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "Дата последнего изменения состояние договора"
              },
              "stateChangedBy" : {
                "description" : "Пользователь, который последним изменил состояние договора",
                "$ref" : "#/definitions/UserSimple"
              },
              "tariff" : {
                "description" : "Страховой продукт",
                "$ref" : "#/definitions/TariffSimple"
              },
              "number" : {
                "type" : "string",
                "description" : "Номер договора",
                "minLength" : 0,
                "maxLength" : 15
              },
              "generalNumber" : {
                "type" : "string",
                "description" : "Номер генерального договора",
                "minLength" : 0,
                "maxLength" : 60
              },
              "date" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "Дата заключения договора"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "Начало периода страхования"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "Окончание периода страхования"
              },
              "period" : {
                "description" : "Период действия страхования",
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "customer" : {
                "description" : "Страхователь",
                "$ref" : "#/definitions/Person"
              },
              "beneficiary" : {
                "description" : "Выгодоприобретатель",
                "$ref" : "#/definitions/Person"
              },
              "insuranceObject" : {
                "description" : "Объект страхования для однообъектного договора",
                "$ref" : "#/definitions/InsuranceObject"
              },
              "insuranceObjects" : {
                "type" : "array",
                "description" : "Объекты страхования для многообъектного договора",
                "items" : {
                  "$ref" : "#/definitions/InsuranceObject"
                }
              },
              "baseTariff" : {
                "type" : "number",
                "description" : "Значение базового тарифа"
              },
              "discount" : {
                "type" : "number",
                "description" : "Скидка"
              },
              "urgencyCoefficient" : {
                "type" : "number",
                "description" : "Коэффициент срочности"
              },
              "payment" : {
                "type" : "number",
                "description" : "Страховой платеж",
                "minimum" : 0.01
              },
              "brokerDiscountedPayment" : {
                "type" : "number",
                "description" : "Страховой платеж без учета комиссии посредника"
              },
              "commission" : {
                "type" : "number",
                "description" : "Комиссия посредника"
              },
              "insurerAccount" : {
                "description" : "Банковский счет. Используется при печати счета фактуры",
                "$ref" : "#/definitions/CompanyAccount"
              },
              "agents" : {
                "type" : "array",
                "description" : "Список агентов, которые участвовали в заключении договора",
                "items" : {
                  "$ref" : "#/definitions/ContractAgent"
                }
              },
              "notes" : {
                "type" : "string",
                "description" : "Комментарий",
                "minLength" : 0,
                "maxLength" : 1024
              },
              "state" : {
                "type" : "string",
                "description" : "Состояние договора",
                "enum" : [ "DELETED", "DRAFT", "SIGNED", "REQUEST", "EMITTED" ]
              },
              "payments" : {
                "type" : "array",
                "description" : "Карта платежей",
                "items" : {
                  "$ref" : "#/definitions/ContractPayment"
                }
              },
              "risks" : {
                "type" : "array",
                "description" : "Риски по договору",
                "items" : {
                  "$ref" : "#/definitions/ContractRisk"
                }
              },
              "customFields" : {
                "type" : "array",
                "description" : "Значения настраиваемых полей",
                "items" : {
                  "$ref" : "#/definitions/CustomFieldValue"
                }
              },
              "multiObject" : {
                "type" : "boolean",
                "description" : "Признак. Многообъектный договор",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор договора во внешней системе"
              }
            },
            "description" : "Базовый класс договора"
          },
          "ContractAgent" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "agent" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "paymentOption" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "calculationKind" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "commission" : {
                "type" : "number"
              }
            }
          },
          "ContractDateToLimit" : {
            "type" : "object",
            "properties" : {
              "value" : {
                "type" : "integer",
                "format" : "int32"
              },
              "datePeriodType" : {
                "type" : "string",
                "enum" : [ "DAYS", "MONTHS" ]
              }
            }
          },
          "ContractPayment" : {
            "type" : "object",
            "required" : [ "created", "date", "payment", "state", "stateChanged" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "date" : {
                "type" : "string",
                "format" : "date-time"
              },
              "created" : {
                "type" : "string",
                "format" : "date-time"
              },
              "stateChanged" : {
                "type" : "string",
                "format" : "date-time"
              },
              "payment" : {
                "type" : "number"
              },
              "number" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "purpose" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 200
              },
              "state" : {
                "type" : "string",
                "enum" : [ "PENDING", "PAID", "CANCELED" ]
              },
              "recipientName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "recipientCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "recipientBankMfo" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 6
              },
              "recipientAccountNumber" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "senderName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "senderCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10
              },
              "senderBankMfo" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 6
              },
              "senderAccountNumber" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              }
            }
          },
          "ContractRisk" : {
            "type" : "object",
            "required" : [ "amountCurrency", "insuranceAmount" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "risk" : {
                "description" : "Риск",
                "$ref" : "#/definitions/Risk"
              },
              "baseTariff" : {
                "type" : "number",
                "description" : "Базовый тариф по риску"
              },
              "insuranceAmount" : {
                "type" : "number",
                "description" : "Страховая сумма по риску"
              },
              "amountCurrency" : {
                "type" : "string",
                "enum" : [ "UAH", "USD", "EUR", "RUB" ]
              },
              "payment" : {
                "type" : "number",
                "description" : "Страховой платеж по риску"
              },
              "customFields" : {
                "type" : "array",
                "description" : "Значения настраиваемых полей",
                "items" : {
                  "$ref" : "#/definitions/CustomFieldValue"
                }
              }
            },
            "description" : "Значения риска по объекту страхования"
          },
          "ContractType" : {
            "type" : "object",
            "required" : [ "active" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 40
              },
              "zeroPadding" : {
                "type" : "integer",
                "format" : "int32"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              },
              "risks" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/Risk"
                }
              }
            },
            "description" : "Тип договора"
          },
          "Country" : {
            "type" : "object",
            "required" : [ "phoneCode" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "nameInEnglish" : {
                "type" : "string"
              },
              "nameInRussian" : {
                "type" : "string"
              },
              "nameInUkrainian" : {
                "type" : "string"
              },
              "alpha2Code" : {
                "type" : "string",
                "minLength" : 2,
                "maxLength" : 2
              },
              "alpha3Code" : {
                "type" : "string",
                "minLength" : 3,
                "maxLength" : 3
              },
              "code" : {
                "type" : "string",
                "minLength" : 3,
                "maxLength" : 3
              },
              "unofficial" : {
                "type" : "boolean",
                "default" : false
              },
              "phoneCode" : {
                "$ref" : "#/definitions/InternationalPhoneCode"
              },
              "parent" : {
                "$ref" : "#/definitions/Country"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              }
            }
          },
          "CoverageTerritory" : {
            "type" : "object",
            "required" : [ "dateFrom" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "shortName" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 32
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 12
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "territoryCountries" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TerritoryCountry"
                },
                "maxItems" : 2147483647,
                "minItems" : 1
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "notes" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 1024
              }
            }
          },
          "CurrencyBasket" : {
            "type" : "object",
            "required" : [ "dateFrom" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "exchangeRates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/CurrencyExchangeRate"
                },
                "maxItems" : 2147483647,
                "minItems" : 1
              }
            }
          },
          "CurrencyExchangeRate" : {
            "type" : "object",
            "required" : [ "dateFrom", "from", "rate", "to" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "from" : {
                "type" : "string",
                "enum" : [ "UAH", "USD", "EUR", "RUB" ]
              },
              "to" : {
                "type" : "string",
                "enum" : [ "UAH", "USD", "EUR", "RUB" ]
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "quantity" : {
                "type" : "integer",
                "format" : "int32",
                "minimum" : 1.0
              },
              "rate" : {
                "type" : "number",
                "minimum" : 0.0
              }
            }
          },
          "CurrentUserInfo" : {
            "type" : "object",
            "properties" : {
              "user" : {
                "readOnly" : true,
                "$ref" : "#/definitions/User"
              },
              "sessionId" : {
                "type" : "string",
                "readOnly" : true
              },
              "hasPolicyTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasPolicyRuTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasVclTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasGreenCardTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "hasTourismTariffs" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "customContractTypes" : {
                "type" : "array",
                "readOnly" : true,
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "CustomAgeField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object"
            } ]
          },
          "CustomContract" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "insuranceAmount" ],
              "properties" : {
                "insuranceAmount" : {
                  "type" : "number"
                }
              },
              "description" : "Базовый класс договора"
            } ]
          },
          "CustomDateField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object"
            } ]
          },
          "CustomEngineVolumeField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object"
            } ]
          },
          "CustomField" : {
            "type" : "object",
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "description" : {
                "type" : "string"
              },
              "archive" : {
                "type" : "boolean",
                "default" : false
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "CustomFieldValue" : {
            "type" : "object",
            "required" : [ "calculationKind" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 60
              },
              "value" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10000
              },
              "calculationKind" : {
                "type" : "string",
                "enum" : [ "NONE", "TARIFF_MULTIPLIER", "TARIFF_ADDITION", "PAYMENT_MULTIPLIER", "PAYMENT_ADDITION" ]
              },
              "coeff" : {
                "type" : "number"
              }
            }
          },
          "CustomListField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object",
              "properties" : {
                "members" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CustomListFieldMember"
                  }
                }
              }
            } ]
          },
          "CustomListFieldMember" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32"
              }
            }
          },
          "CustomNumberField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object",
              "properties" : {
                "minValue" : {
                  "type" : "number"
                },
                "maxValue" : {
                  "type" : "number"
                },
                "fraction" : {
                  "type" : "integer",
                  "format" : "int32",
                  "minimum" : 0.0,
                  "maximum" : 6.0
                }
              }
            } ]
          },
          "CustomObjectCountField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object"
            } ]
          },
          "CustomTextField" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CustomField"
            }, {
              "type" : "object",
              "properties" : {
                "multiline" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minLength" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "maxLength" : {
                  "type" : "integer",
                  "format" : "int32",
                  "maximum" : 10000.0
                },
                "mask" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 400
                },
                "defaultValue" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 400
                }
              }
            } ]
          },
          "CustomerFieldSettings" : {
            "type" : "object",
            "required" : [ "address", "birthDate", "code", "documentDate", "documentIssuedBy", "documentNumber", "documentSeries", "mail", "name", "nameFirst", "nameMiddle", "phone" ],
            "properties" : {
              "code" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "allowUndefinedCode" : {
                "type" : "boolean",
                "default" : false
              },
              "codeDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 10,
                "pattern" : "[0-9]*"
              },
              "name" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "nameDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "nameFirst" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "nameMiddle" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "address" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "addressDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "useAddressHelper" : {
                "type" : "boolean",
                "default" : false
              },
              "phone" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "phoneDefault" : {
                "type" : "string",
                "minLength" : 12,
                "maxLength" : 13
              },
              "mail" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "mailDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "birthDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "birthDateDefault" : {
                "type" : "string",
                "format" : "date-time"
              },
              "documentTypes" : {
                "type" : "array",
                "items" : {
                  "type" : "string",
                  "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
                }
              },
              "documentTypeDefault" : {
                "type" : "string",
                "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
              },
              "documentSeries" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentSeriesDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 4
              },
              "documentNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentNumberDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "documentDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentDateDefault" : {
                "type" : "string",
                "format" : "date-time"
              },
              "documentIssuedBy" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "documentIssuedByDefault" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "useDocumentIssuedByHelper" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "GreenCard" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "properties" : {
                "vehicleType" : {
                  "type" : "string",
                  "enum" : [ "A", "B", "C", "E", "F" ]
                },
                "coverageArea" : {
                  "type" : "string",
                  "enum" : [ "BMR", "EUROPE" ]
                },
                "verificationCode" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 15
                }
              },
              "description" : "Полис \"Зеленая карта\""
            } ]
          },
          "GreenCardTariffChoose" : {
            "type" : "object",
            "properties" : {
              "tariff" : {
                "readOnly" : true,
                "$ref" : "#/definitions/TariffGreenCard"
              },
              "payment" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedPayment" : {
                "type" : "number",
                "readOnly" : true
              },
              "commission" : {
                "type" : "number"
              },
              "discountedCommission" : {
                "type" : "number"
              },
              "approx" : {
                "type" : "boolean",
                "default" : false
              },
              "canCreated" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "commissionForEurope" : {
                "type" : "number"
              },
              "commissionForBMR" : {
                "type" : "number"
              }
            }
          },
          "InsuranceObject" : {
            "type" : "object",
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор объекта в системе EWA"
              },
              "price" : {
                "type" : "number"
              },
              "insuranceAmount" : {
                "type" : "number",
                "description" : "Страховая сумма"
              },
              "payment" : {
                "type" : "number"
              },
              "risks" : {
                "type" : "array",
                "description" : "Риски по объекту",
                "items" : {
                  "$ref" : "#/definitions/ContractRisk"
                }
              },
              "customFields" : {
                "type" : "array",
                "description" : "Значения настраиваемых полей",
                "items" : {
                  "$ref" : "#/definitions/CustomFieldValue"
                }
              }
            },
            "description" : "Базовый класс объекта страхования"
          },
          "InsuranceObjectAuto" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "required" : [ "dontHaveBodyNumber", "dontHaveStateNumber" ],
              "properties" : {
                "category" : {
                  "type" : "string",
                  "description" : "Категория авто",
                  "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
                },
                "model" : {
                  "description" : "Модель авто",
                  "$ref" : "#/definitions/AutoModel"
                },
                "modelText" : {
                  "type" : "string",
                  "description" : "Модель авто текстом",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "complectation" : {
                  "type" : "string",
                  "description" : "Комплектация авто"
                },
                "bodyNumber" : {
                  "type" : "string",
                  "description" : "Номер кузова (VIN)",
                  "minLength" : 0,
                  "maxLength" : 17
                },
                "dontHaveBodyNumber" : {
                  "type" : "boolean",
                  "description" : "Признак. Авто не имеет номера кузова (VIN)",
                  "default" : false
                },
                "stateNumber" : {
                  "type" : "string",
                  "description" : "Государственный номер",
                  "minLength" : 0,
                  "maxLength" : 10
                },
                "dontHaveStateNumber" : {
                  "type" : "boolean",
                  "description" : "Признак. Авто не имеет государственный номер",
                  "default" : false
                },
                "registrationPlace" : {
                  "description" : "Место регистрации авто",
                  "$ref" : "#/definitions/Place"
                },
                "outsideUkraine" : {
                  "type" : "boolean",
                  "description" : "Признак. Авто зарегистрировано за пределами Украины",
                  "default" : false
                },
                "registrationType" : {
                  "type" : "string",
                  "description" : "Тип регистрации авто",
                  "enum" : [ "PERMANENT_WITHOUT_OTK", "PERMANENT_WITH_OTK", "NOT_REGISTERED", "TEMPORARY", "TEMPORARY_ENTRANCE" ]
                },
                "otkDate" : {
                  "type" : "string",
                  "format" : "date-time",
                  "description" : "Дата регистрации. Смысл меняется в зависимости от типа регистрации:<ul><li>игнорируется, если тип \"Постоянная регистрация (без ОТК)\"<li>дата следующего ОТК, если тип \"Постоянная регистрация (c ОТК)\"<li>дата будущей регистрации, если тип \"Без регистрации\"<li>дата окончания регистрации, если тип \"Временная регистрация\"<li>дата выезда, если тип \"Временный вьезд\"</ul>"
                },
                "year" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Год выпуска"
                },
                "engineVolume" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Объем двигателя"
                },
                "autoColor" : {
                  "type" : "string",
                  "description" : "Цвет",
                  "minLength" : 0,
                  "maxLength" : 50
                },
                "autoOwner" : {
                  "type" : "string",
                  "description" : "Владелец",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "autoRegistrationDocument" : {
                  "type" : "string",
                  "description" : "Серия и номер документа о регистрации авто",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "autoCreditDocument" : {
                  "type" : "string",
                  "description" : "Серия и номер кредитного договора",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "autoPledgeDocument" : {
                  "type" : "string",
                  "description" : "Серия и номер договора залога",
                  "minLength" : 0,
                  "maxLength" : 60
                }
              },
              "description" : "Объект страхования - автомобиль"
            } ]
          },
          "InsuranceObjectPerson" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "properties" : {
                "phone" : {
                  "type" : "string"
                },
                "nameLast" : {
                  "type" : "string"
                },
                "nameFirst" : {
                  "type" : "string"
                },
                "nameMiddle" : {
                  "type" : "string"
                },
                "birthDate" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dontHaveCode" : {
                  "type" : "boolean",
                  "default" : false
                },
                "document" : {
                  "$ref" : "#/definitions/PersonDocument"
                },
                "code" : {
                  "type" : "string"
                },
                "email" : {
                  "type" : "string"
                },
                "address" : {
                  "type" : "string"
                },
                "name" : {
                  "type" : "string"
                }
              },
              "description" : "Базовый класс объекта страхования"
            } ]
          },
          "InsuranceObjectProperty" : {
            "allOf" : [ {
              "$ref" : "#/definitions/InsuranceObject"
            }, {
              "type" : "object",
              "properties" : {
                "description" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "place" : {
                  "$ref" : "#/definitions/Place"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "totalSpace" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "livingSpace" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "floor" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "numberOfFloors" : {
                  "type" : "integer",
                  "format" : "int32",
                  "minimum" : 1.0
                },
                "propertyOwner" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                }
              },
              "description" : "Базовый класс объекта страхования"
            } ]
          },
          "Insurer" : {
            "allOf" : [ {
              "$ref" : "#/definitions/CompanySimple"
            }, {
              "type" : "object",
              "required" : [ "dateFrom" ],
              "properties" : {
                "evoUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "smsName" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 11,
                  "pattern" : "^[a-zA-Z0-9!@'\"#\\$:\\._-]+$"
                },
                "address" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 100
                },
                "hotlinePhone" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 60
                },
                "homePage" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 120,
                  "pattern" : "(^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$)?"
                },
                "paymentOptions" : {
                  "type" : "string"
                },
                "calculationKinds" : {
                  "type" : "string"
                },
                "accounts" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyAccount"
                  }
                },
                "emails" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/CompanyEmail"
                  }
                },
                "contractTypes" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ContractType"
                  }
                },
                "dateFrom" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "dateTo" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "hasDataHelper" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dataHelperParameters" : {
                  "type" : "string"
                },
                "closePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "exportClosePeriod" : {
                  "type" : "string",
                  "format" : "date-time"
                },
                "insurerCode" : {
                  "type" : "string",
                  "minLength" : 3,
                  "maxLength" : 3
                }
              }
            } ]
          },
          "InternationalPhoneCode" : {
            "type" : "object",
            "properties" : {
              "code" : {
                "type" : "string"
              },
              "phoneLength" : {
                "type" : "integer",
                "format" : "int32",
                "minimum" : 1.0
              }
            }
          },
          "MiscFieldSettings" : {
            "type" : "object",
            "required" : [ "agent", "generalNumber", "insuranceObjectPrice", "insurerAccount", "number", "paymentDate", "paymentNumber" ],
            "properties" : {
              "number" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "generalNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "agent" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "insurerAccount" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "paymentDate" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "paymentNumber" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "insuranceObjectPrice" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "Person" : {
            "type" : "object",
            "required" : [ "dontHaveCode", "legal" ],
            "properties" : {
              "code" : {
                "type" : "string",
                "description" : "Идентификационный номер налогоплательщика",
                "minLength" : 0,
                "maxLength" : 10,
                "pattern" : "[0-9]*"
              },
              "dontHaveCode" : {
                "type" : "boolean",
                "description" : "Признак. Идентификационный номер налогоплательщика отсутствует",
                "default" : false
              },
              "name" : {
                "type" : "string",
                "description" : "Полное наименование",
                "minLength" : 0,
                "maxLength" : 255
              },
              "nameLast" : {
                "type" : "string",
                "description" : "Фамилия",
                "minLength" : 0,
                "maxLength" : 100
              },
              "nameFirst" : {
                "type" : "string",
                "description" : "Имя",
                "minLength" : 0,
                "maxLength" : 75
              },
              "nameMiddle" : {
                "type" : "string",
                "description" : "Отчество",
                "minLength" : 0,
                "maxLength" : 75
              },
              "address" : {
                "type" : "string",
                "description" : "Адрес",
                "minLength" : 0,
                "maxLength" : 255
              },
              "phone" : {
                "type" : "string",
                "description" : "Телефон",
                "minLength" : 12,
                "maxLength" : 13
              },
              "email" : {
                "type" : "string",
                "description" : "Адрес электронной почты",
                "minLength" : 0,
                "maxLength" : 50
              },
              "birthDate" : {
                "type" : "string",
                "format" : "date-time",
                "description" : "День рождения"
              },
              "document" : {
                "description" : "Документ, удостоверяющий личность",
                "$ref" : "#/definitions/PersonDocument"
              },
              "legal" : {
                "type" : "boolean",
                "description" : "Признак. Юридическое лицо",
                "default" : false
              }
            },
            "description" : "Описание контрагента в договоре"
          },
          "PersonDocument" : {
            "type" : "object",
            "properties" : {
              "type" : {
                "type" : "string",
                "enum" : [ "PASSPORT", "DRIVING_LICENSE", "RESIDENCE_PERMIT", "VETERAN_CERTIFICATE", "REGISTRATION_CARD", "EXTERNAL_PASSPORT", "FOREIGN_PASSPORT", "PENSION_CERTIFICATE", "DISABILITY_CERTIFICATE", "CHERNOBYL_CERTIFICATE" ]
              },
              "series" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 4
              },
              "number" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "date" : {
                "type" : "string",
                "format" : "date-time"
              },
              "issuedBy" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              }
            }
          },
          "Place" : {
            "type" : "object",
            "required" : [ "cdbMtibu", "country", "zone" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "country" : {
                "type" : "string",
                "enum" : [ "UA", "RU" ]
              },
              "placeCode" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "nameRus" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 50
              },
              "nameFull" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "cdbMtibu" : {
                "type" : "boolean",
                "default" : false
              },
              "cdbMtibuCode" : {
                "type" : "integer",
                "format" : "int64"
              }
            }
          },
          "Policy" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "bonusMalus", "drivingExpLessThreeYears", "franchise", "k1", "k2", "k3", "k4", "k5", "k6", "privilege", "taxi", "usageMonths" ],
              "properties" : {
                "usageMonths" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Месяца неиспользования. Битовая карта, где (2048)<sub>10</sub>=(100000000000)<sub>2</sub> означает, что первый месяц не используется"
                },
                "franchise" : {
                  "type" : "number",
                  "description" : "Франшиза"
                },
                "k1" : {
                  "type" : "number",
                  "description" : "Коэффициент k1"
                },
                "k2" : {
                  "type" : "number",
                  "description" : "Коэффициент k2"
                },
                "k3" : {
                  "type" : "number",
                  "description" : "Коэффициент k3"
                },
                "k4" : {
                  "type" : "number",
                  "description" : "Коэффициент k4"
                },
                "k5" : {
                  "type" : "number",
                  "description" : "Коэффициент k5"
                },
                "k6" : {
                  "type" : "number",
                  "description" : "Коэффициент k6"
                },
                "bonusMalus" : {
                  "type" : "number",
                  "description" : "Коэффициент бонус-малус"
                },
                "privilege" : {
                  "type" : "number",
                  "description" : "Коэффициент льготы"
                },
                "privilegeType" : {
                  "type" : "string",
                  "description" : "Тип льготы",
                  "enum" : [ "VETERAN", "DISABLED", "CHERNOBYLETS", "PENSIONER" ]
                },
                "drivingExpLessThreeYears" : {
                  "type" : "boolean",
                  "description" : "Признак. Стаж вождения менее трех лет",
                  "default" : false
                },
                "taxi" : {
                  "type" : "boolean",
                  "description" : "Признак. Авто используется как такси",
                  "default" : false
                },
                "stickerNumber" : {
                  "type" : "string",
                  "description" : "Стикер",
                  "minLength" : 0,
                  "maxLength" : 10
                },
                "requestUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                },
                "policyUrl" : {
                  "type" : "string",
                  "minLength" : 0,
                  "maxLength" : 255
                }
              },
              "description" : "Полис \"ОСАГО\""
            } ]
          },
          "PolicyFieldSettings" : {
            "type" : "object",
            "required" : [ "bonusMalus", "drivingExp", "k2", "k3", "k4", "sticker", "usageMonths" ],
            "properties" : {
              "usageMonths" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "drivingExp" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k2" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k3" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "k4" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "bonusMalus" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "sticker" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "PolicyRu" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "bonusMalus", "category", "driversCount", "enginePower", "taxi" ],
              "properties" : {
                "category" : {
                  "type" : "string",
                  "description" : "Категория авто",
                  "enum" : [ "A", "B", "C1", "C2", "D1", "D2", "E", "F", "G" ]
                },
                "enginePower" : {
                  "type" : "string",
                  "description" : "Мощность двигателя",
                  "enum" : [ "LESS_50", "BETWEEN_50_70", "BETWEEN_70_100", "BETWEEN_100_120", "BETWEEN_120_150", "GREATHER_150" ]
                },
                "minSeniorityAndAge" : {
                  "type" : "string",
                  "enum" : [ "LESS_22_LESS_3", "GREATHER_22_LESS_3", "LESS_22_GREATHER_3", "GREATHER_22_GREATHER_3" ]
                },
                "driversCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "bonusMalus" : {
                  "type" : "number"
                },
                "taxi" : {
                  "type" : "boolean",
                  "default" : false
                },
                "driver1" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver2" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver3" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver4" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "driver5" : {
                  "$ref" : "#/definitions/PolicyRuDriver"
                },
                "kTerritory" : {
                  "type" : "number"
                },
                "kEngine" : {
                  "type" : "number"
                },
                "kSeniority" : {
                  "type" : "number"
                },
                "kViolations" : {
                  "type" : "number"
                }
              },
              "description" : "Полис \"ОСАГО\" (Россия)"
            } ]
          },
          "PolicyRuDriver" : {
            "type" : "object",
            "properties" : {
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              },
              "license" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 20
              }
            }
          },
          "PolicyRuFieldSettings" : {
            "type" : "object",
            "required" : [ "bonusMalus" ],
            "properties" : {
              "bonusMalus" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "PropertyFieldSettings" : {
            "type" : "object",
            "required" : [ "propertyAddress", "propertyDescription", "propertyFloor", "propertyLivingSpace", "propertyNumberOfFloors", "propertyOwner", "propertyPlace", "propertyTotalSpace" ],
            "properties" : {
              "propertyPlace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyAddress" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyTotalSpace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyLivingSpace" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyFloor" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyNumberOfFloors" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyOwner" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "propertyDescription" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              }
            }
          },
          "ReportTemplate" : {
            "type" : "object",
            "required" : [ "dateFrom", "name", "templateType" ],
            "discriminator" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "notes" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 1024
              },
              "hasDraft" : {
                "type" : "boolean",
                "default" : false
              },
              "parameters" : {
                "type" : "string"
              },
              "company" : {
                "$ref" : "#/definitions/Company"
              },
              "templateType" : {
                "type" : "string",
                "enum" : [ "JASPER", "HTML" ]
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "contractTypes" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ContractType"
                }
              }
            }
          },
          "ReportTemplateWithContent" : {
            "allOf" : [ {
              "$ref" : "#/definitions/ReportTemplate"
            }, {
              "type" : "object",
              "properties" : {
                "content" : {
                  "type" : "string"
                }
              }
            } ]
          },
          "Risk" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор риска"
              },
              "shortName" : {
                "type" : "string",
                "description" : "Краткое название риска",
                "minLength" : 0,
                "maxLength" : 50
              },
              "name" : {
                "type" : "string",
                "description" : "Название риска",
                "minLength" : 0,
                "maxLength" : 100
              },
              "required" : {
                "type" : "boolean",
                "description" : "Риск обязателен для использования в страховом продукте",
                "default" : false
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32",
                "description" : "Позиция отображения"
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор риска во внешней системе"
              }
            },
            "description" : "Риски"
          },
          "SalePointSimple" : {
            "type" : "object",
            "required" : [ "company" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "идентификатор точки продаж"
              },
              "company" : {
                "description" : "компания, которому принадлежит точка продаж",
                "$ref" : "#/definitions/CompanySimple"
              },
              "code" : {
                "type" : "string",
                "description" : "код точки продаж",
                "minLength" : 0,
                "maxLength" : 100
              },
              "name" : {
                "type" : "string",
                "description" : "наименование точки продаж",
                "minLength" : 0,
                "maxLength" : 120
              },
              "namePrint" : {
                "type" : "string",
                "description" : "наименование для печати",
                "minLength" : 0,
                "maxLength" : 120
              },
              "lft" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "левое значение NestedSet"
              },
              "rgt" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "правое значение NestedSet"
              },
              "path" : {
                "type" : "string",
                "description" : "путь в дереве (id через запятую)"
              },
              "paymentsAvailable" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffCustom" : {
            "type" : "object",
            "required" : [ "baseTariff", "blankUsage", "calcType", "company", "contractDateLimitType", "contractType", "customerCategory", "dateFrom", "dateFromLimitType", "insuranceObjectType", "insurer", "maxObjectCount", "objectsCalcType", "state" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "risksSupported" : {
                "type" : "boolean",
                "default" : false
              },
              "risksOnContract" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор тарифа во внешней системе системе (в системе компании)"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "periods" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPeriod"
                }
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "risks" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffRisk"
                }
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calcType" : {
                "type" : "string",
                "enum" : [ "CALC_PAYMENT", "CALC_AMOUNT", "CALC_TARIFF" ]
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              },
              "baseTariff" : {
                "type" : "number"
              },
              "freeInsuranceAmount" : {
                "type" : "boolean",
                "default" : false
              },
              "minInsuranceAmount" : {
                "type" : "number"
              },
              "maxInsuranceAmount" : {
                "type" : "number"
              },
              "defaultInsuranceAmount" : {
                "type" : "number"
              },
              "insuranceAmounts" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomAmount"
                }
              },
              "commission" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "brokerDiscount" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              }
            },
            "description" : "Базовый класс тарифов"
          },
          "TariffCustomAmount" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "risk" : {
                "$ref" : "#/definitions/Risk"
              },
              "amount" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              },
              "description" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 255
              }
            }
          },
          "TariffCustomField" : {
            "type" : "object",
            "required" : [ "calculationKind", "contractBlock", "customField", "fieldVisibility" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "customField" : {
                "$ref" : "#/definitions/CustomField"
              },
              "risk" : {
                "$ref" : "#/definitions/Risk"
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32"
              },
              "fieldVisibility" : {
                "type" : "string",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "contractBlock" : {
                "type" : "string",
                "enum" : [ "CALCULATION_CONTRACT", "CALCULATION_OBJECT", "CALCULATION_RISK", "CUSTOMER", "BENEFICIARY", "INSURANCE_OBJECT", "OTHER" ]
              },
              "calculationKind" : {
                "type" : "string",
                "enum" : [ "NONE", "TARIFF_MULTIPLIER", "TARIFF_ADDITION", "PAYMENT_MULTIPLIER", "PAYMENT_ADDITION" ]
              },
              "members" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomFieldListMember"
                }
              }
            }
          },
          "TariffCustomFieldListMember" : {
            "type" : "object",
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 100
              },
              "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 150
              },
              "defaultMember" : {
                "type" : "boolean",
                "default" : false
              },
              "coeff" : {
                "type" : "number"
              },
              "sortOrder" : {
                "type" : "integer",
                "format" : "int32"
              }
            }
          },
          "TariffCustomFieldRangeMember" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffCustomFieldListMember"
            }, {
              "type" : "object",
              "properties" : {
                "min" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "max" : {
                  "type" : "integer",
                  "format" : "int32"
                }
              }
            } ]
          },
          "TariffGreenCard" : {
            "type" : "object",
            "required" : [ "blankUsage", "company", "contractDateLimitType", "customerCategory", "dateFrom", "dateFromLimitType", "insuranceObjectType", "insurer", "maxObjectCount", "objectsCalcType", "state" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "risksSupported" : {
                "type" : "boolean",
                "default" : false
              },
              "risksOnContract" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор тарифа во внешней системе системе (в системе компании)"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "risks" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffRisk"
                }
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "commissionForEurope" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "commissionForBMR" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "maxDiscount" : {
                "type" : "number"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Базовый класс тарифов"
          },
          "TariffPeriod" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "period" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              }
            }
          },
          "TariffPolicy" : {
            "type" : "object",
            "required" : [ "blankUsage", "company", "contractDateLimitType", "customerCategory", "dateFrom", "dateFromLimitType", "franchise", "insuranceObjectType", "insurer", "maxObjectCount", "medium", "minBonusMalus", "objectsCalcType", "policySettings", "state" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "risksSupported" : {
                "type" : "boolean",
                "default" : false
              },
              "risksOnContract" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор тарифа во внешней системе системе (в системе компании)"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "periods" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPeriod"
                }
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "risks" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffRisk"
                }
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "franchise" : {
                "type" : "number"
              },
              "taxi" : {
                "type" : "boolean",
                "default" : false
              },
              "minBonusMalus" : {
                "type" : "number"
              },
              "medium" : {
                "type" : "string",
                "enum" : [ "PAPER", "ELECTRONIC" ]
              },
              "k2Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK2"
                }
              },
              "k3Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK3"
                }
              },
              "k4Values" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPolicyK4"
                }
              },
              "maxDiscount" : {
                "type" : "number"
              },
              "commission" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "brokerDiscount" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "useCdbMtibu" : {
                "type" : "boolean",
                "default" : false
              },
              "policySettings" : {
                "$ref" : "#/definitions/PolicyFieldSettings"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Тариф для ОСАГО"
          },
          "TariffPolicyChoose" : {
            "type" : "object",
            "properties" : {
              "tariff" : {
                "readOnly" : true,
                "$ref" : "#/definitions/TariffPolicy"
              },
              "payment" : {
                "type" : "number",
                "readOnly" : true
              },
              "commission" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedPayment" : {
                "type" : "number",
                "readOnly" : true
              },
              "discountedCommission" : {
                "type" : "number",
                "readOnly" : true
              },
              "approx" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "canCreated" : {
                "type" : "boolean",
                "readOnly" : true,
                "default" : false
              },
              "actionDescription" : {
                "type" : "string",
                "readOnly" : true
              },
              "discountDescription" : {
                "type" : "string",
                "readOnly" : true
              }
            }
          },
          "TariffPolicyK2" : {
            "type" : "object",
            "required" : [ "zone" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffPolicyK3" : {
            "type" : "object",
            "required" : [ "autoCategory" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffPolicyK4" : {
            "type" : "object",
            "required" : [ "maxValueAbove3", "maxValueLess3", "minValueAbove3", "minValueLess3" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "minValueLess3" : {
                "type" : "number"
              },
              "maxValueLess3" : {
                "type" : "number"
              },
              "minValueAbove3" : {
                "type" : "number"
              },
              "maxValueAbove3" : {
                "type" : "number"
              }
            }
          },
          "TariffPolicyRu" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffSimple"
            }, {
              "type" : "object",
              "required" : [ "blankUsage", "contractDateLimitType", "customerCategory", "dateFromLimitType", "insuranceObjectType", "maxObjectCount", "minBonusMalus", "policyRuFieldSettings", "state" ],
              "properties" : {
                "customerCategory" : {
                  "type" : "string",
                  "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
                },
                "reportTemplates" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ReportTemplate"
                  }
                },
                "customFields" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomField"
                  }
                },
                "contractDateLimitType" : {
                  "type" : "string",
                  "enum" : [ "TODAY", "BEFORE_TODAY" ]
                },
                "contractDateLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "dateFromLimitType" : {
                  "type" : "string",
                  "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
                },
                "dateFromLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "freeDateTo" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "periods" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPeriod"
                  }
                },
                "roundPayment" : {
                  "type" : "boolean",
                  "default" : false
                },
                "actionDescription" : {
                  "type" : "string"
                },
                "discountDescription" : {
                  "type" : "string"
                },
                "blankUsage" : {
                  "type" : "string",
                  "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
                },
                "blankAutoSelect" : {
                  "type" : "boolean",
                  "default" : false
                },
                "blankSearchUp" : {
                  "type" : "boolean",
                  "default" : false
                },
                "hasBeneficiary" : {
                  "type" : "boolean",
                  "default" : false
                },
                "proportionalUrgency" : {
                  "type" : "boolean",
                  "default" : false
                },
                "urgencyValues" : {
                  "type" : "array",
                  "items" : {
                    "type" : "number"
                  }
                },
                "state" : {
                  "type" : "string",
                  "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
                },
                "hasPartnerTariff" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dateFromWithTime" : {
                  "type" : "boolean",
                  "default" : false
                },
                "insuranceObjectType" : {
                  "type" : "string",
                  "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
                },
                "maxObjectCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "risks" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffRisk"
                  }
                },
                "customerSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "beneficiarySettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "miscSettings" : {
                  "$ref" : "#/definitions/MiscFieldSettings"
                },
                "autoSettings" : {
                  "$ref" : "#/definitions/AutoFieldSettings"
                },
                "propertySettings" : {
                  "$ref" : "#/definitions/PropertyFieldSettings"
                },
                "personSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "paymentOptions" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calculationKinds" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "taxi" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minBonusMalus" : {
                  "type" : "number"
                },
                "baseTariffs" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPolicyRuBT"
                  }
                },
                "policyRuFieldSettings" : {
                  "$ref" : "#/definitions/PolicyRuFieldSettings"
                }
              },
              "description" : "Базовый класс тарифов"
            } ]
          },
          "TariffPolicyRuBT" : {
            "type" : "object",
            "required" : [ "autoCategory" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A", "B", "C1", "C2", "D1", "D2", "E", "F", "G" ]
              },
              "minValue" : {
                "type" : "number"
              },
              "maxValue" : {
                "type" : "number"
              },
              "active" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "TariffRisk" : {
            "type" : "object",
            "required" : [ "usage" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор"
              },
              "risk" : {
                "description" : "Риск",
                "$ref" : "#/definitions/Risk"
              },
              "usage" : {
                "type" : "string",
                "description" : "Настройки обязательности использования",
                "enum" : [ "HIDDEN", "VISIBLE", "REQUIRED", "HIDDEN_WITH_DEFAULT" ]
              },
              "byDefault" : {
                "type" : "boolean",
                "default" : false
              },
              "inCurrency" : {
                "type" : "boolean",
                "default" : false
              },
              "baseTariff" : {
                "type" : "number",
                "description" : "Базовый тариф по риску"
              },
              "minInsuranceAmount" : {
                "type" : "number",
                "description" : "Мин. значение страховой суммы (при произвольном значении) по риску"
              },
              "maxInsuranceAmount" : {
                "type" : "number",
                "description" : "Макс. значение страховой суммы (при произвольном значении) по риску"
              },
              "defaultInsuranceAmount" : {
                "type" : "number",
                "description" : "Страховая сумма по умолчанию для риска"
              }
            },
            "description" : "Риски по тарифам"
          },
          "TariffSimple" : {
            "type" : "object",
            "required" : [ "company", "dateFrom", "insurer", "objectsCalcType" ],
            "discriminator" : "type",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "risksSupported" : {
                "type" : "boolean",
                "default" : false
              },
              "risksOnContract" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор тарифа во внешней системе системе (в системе компании)"
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Минимизированная информация по тарифу"
          },
          "TariffTourism" : {
            "allOf" : [ {
              "$ref" : "#/definitions/TariffSimple"
            }, {
              "type" : "object",
              "required" : [ "blankUsage", "contractDateLimitType", "currencyBasket", "customerCategory", "dateFromLimitType", "insuranceObjectType", "maxObjectCount", "multivisaCoeff", "multivisaSupport", "state" ],
              "properties" : {
                "customerCategory" : {
                  "type" : "string",
                  "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
                },
                "reportTemplates" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/ReportTemplate"
                  }
                },
                "customFields" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffCustomField"
                  }
                },
                "contractDateLimitType" : {
                  "type" : "string",
                  "enum" : [ "TODAY", "BEFORE_TODAY" ]
                },
                "contractDateLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "dateFromLimitType" : {
                  "type" : "string",
                  "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
                },
                "dateFromLimit" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "freeDateTo" : {
                  "type" : "boolean",
                  "default" : false
                },
                "minDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateTo" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "periods" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffPeriod"
                  }
                },
                "roundPayment" : {
                  "type" : "boolean",
                  "default" : false
                },
                "actionDescription" : {
                  "type" : "string"
                },
                "discountDescription" : {
                  "type" : "string"
                },
                "blankUsage" : {
                  "type" : "string",
                  "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
                },
                "blankAutoSelect" : {
                  "type" : "boolean",
                  "default" : false
                },
                "blankSearchUp" : {
                  "type" : "boolean",
                  "default" : false
                },
                "hasBeneficiary" : {
                  "type" : "boolean",
                  "default" : false
                },
                "proportionalUrgency" : {
                  "type" : "boolean",
                  "default" : false
                },
                "urgencyValues" : {
                  "type" : "array",
                  "items" : {
                    "type" : "number"
                  }
                },
                "state" : {
                  "type" : "string",
                  "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
                },
                "hasPartnerTariff" : {
                  "type" : "boolean",
                  "default" : false
                },
                "dateFromWithTime" : {
                  "type" : "boolean",
                  "default" : false
                },
                "insuranceObjectType" : {
                  "type" : "string",
                  "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
                },
                "maxObjectCount" : {
                  "type" : "integer",
                  "format" : "int32"
                },
                "risks" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/TariffRisk"
                  }
                },
                "customerSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "beneficiarySettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "miscSettings" : {
                  "$ref" : "#/definitions/MiscFieldSettings"
                },
                "autoSettings" : {
                  "$ref" : "#/definitions/AutoFieldSettings"
                },
                "propertySettings" : {
                  "$ref" : "#/definitions/PropertyFieldSettings"
                },
                "personSettings" : {
                  "$ref" : "#/definitions/CustomerFieldSettings"
                },
                "paymentOptions" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "calculationKinds" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "multivisaSupport" : {
                  "type" : "string",
                  "description" : "Доступность применения мультивизы",
                  "enum" : [ "SINGLE_ENTRY", "MULTIVISA", "CAN_BE_SELECTED" ]
                },
                "multivisaCoeff" : {
                  "type" : "number",
                  "description" : "Коэффициент для мультивизы, если разрешен выбор"
                },
                "freeCoverageDays" : {
                  "type" : "boolean",
                  "description" : "Произвольный выбор кол-ва дней покрытия. <code>true</code> если Произвольный выбор",
                  "default" : false
                },
                "minCoverageDays" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Минимальное кол-во дней покрытия при произвольном выборе кол-ва дней."
                },
                "maxCoverageDays" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Максимальное кол-во дней покрытия при произвольном выборе кол-ва дней."
                },
                "defaultCoverageDays" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Кол-во дней покрытия по умолчанию"
                },
                "coverageDaysValues" : {
                  "type" : "array",
                  "description" : "Возможные кол-ва дней покрытия при выборе кол-ва дней из списка",
                  "items" : {
                    "type" : "integer",
                    "format" : "int32"
                  }
                },
                "contractDaysCorrective" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Корректировка даты окончания договора для одиночной поездки.<br/>Некоторые страховые компании при одиночных поездках в страну шенгена увеличивают срок действия договорана несколько дней относительно кол-ва дней покрытия (обычно 15)"
                },
                "minDateToMultivisa" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "maxDateToMultivisa" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "defaultDateToMultivisa" : {
                  "$ref" : "#/definitions/ContractDateToLimit"
                },
                "currencyBasket" : {
                  "description" : "Используемый набор курсов валют",
                  "$ref" : "#/definitions/CurrencyBasket"
                },
                "insuranceAmounts" : {
                  "type" : "array",
                  "description" : "Страховые суммы/платежи",
                  "items" : {
                    "$ref" : "#/definitions/TariffTourismAmount"
                  }
                },
                "coverageTerritories" : {
                  "type" : "array",
                  "description" : "Территории покрытия",
                  "items" : {
                    "$ref" : "#/definitions/TariffTourismCoverageTerritory"
                  }
                },
                "commission" : {
                  "type" : "number",
                  "description" : "Комиссия",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                },
                "brokerDiscount" : {
                  "type" : "number",
                  "description" : "Скидка посредника",
                  "minimum" : 0.0,
                  "maximum" : 1.0
                }
              },
              "description" : "Тариф для туризма (ВЗР)"
            } ]
          },
          "TariffTourismAmount" : {
            "type" : "object",
            "required" : [ "maxDays", "minDays" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор"
              },
              "risk" : {
                "description" : "Риск",
                "$ref" : "#/definitions/Risk"
              },
              "amount" : {
                "type" : "number",
                "description" : "Страховая сумма"
              },
              "currencyPayment" : {
                "type" : "boolean",
                "description" : "Платеж задается в валюте СС или в гривнах. <code>true</code> если платеж в валюте СС.",
                "default" : false
              },
              "perDay" : {
                "type" : "boolean",
                "description" : "Платеж задается на один день или на кол-во дней действия договора. <code>true</code> если платеж задается на 1 день.",
                "default" : false
              },
              "useTerritoryCoeff" : {
                "type" : "boolean",
                "description" : "Умножать ли на коэффициент территории. <code>true</code> если умножать.",
                "default" : false
              },
              "minDays" : {
                "type" : "integer",
                "format" : "int32",
                "description" : "Минимальное кол-во дней действия договора."
              },
              "maxDays" : {
                "type" : "integer",
                "format" : "int32",
                "description" : "Максимальное кол-во дней действия договора."
              },
              "payment" : {
                "type" : "number",
                "description" : "Платеж."
              }
            },
            "description" : "Страховые суммы-платежи в тарифе по туризму"
          },
          "TariffTourismCoverageTerritory" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор"
              },
              "coverageTerritory" : {
                "description" : "Территория покрытия",
                "$ref" : "#/definitions/CoverageTerritory"
              },
              "coefficient" : {
                "type" : "number",
                "description" : "Коэффициент."
              }
            },
            "description" : "Территории покрытия в тарифе по туризму"
          },
          "TariffVcl" : {
            "type" : "object",
            "required" : [ "blankUsage", "company", "contractDateLimitType", "customerCategory", "dateFrom", "dateFromLimitType", "insuranceObjectType", "insurer", "maxObjectCount", "objectsCalcType", "state" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор тарифа"
              },
              "name" : {
                "type" : "string",
                "description" : "Название тарифа",
                "minLength" : 0,
                "maxLength" : 100
              },
              "company" : {
                "description" : "компания, которой принадлежит тариф",
                "$ref" : "#/definitions/CompanySimple"
              },
              "insurer" : {
                "description" : "страховщик для тарифа брокера",
                "$ref" : "#/definitions/CompanySimple"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "objectsCalcType" : {
                "type" : "string",
                "enum" : [ "BY_CONTRACT_WITHOUT_OBJECT_COUNT", "BY_CONTRACT_MULTIPLIED_ON_OBJECT_COUNT", "BY_OBJECT" ]
              },
              "risksSupported" : {
                "type" : "boolean",
                "default" : false
              },
              "risksOnContract" : {
                "type" : "boolean",
                "default" : false
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор тарифа во внешней системе системе (в системе компании)"
              },
              "customerCategory" : {
                "type" : "string",
                "enum" : [ "NATURAL", "LEGAL", "PRIVILEGED" ]
              },
              "reportTemplates" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/ReportTemplate"
                }
              },
              "customFields" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffCustomField"
                }
              },
              "contractDateLimitType" : {
                "type" : "string",
                "enum" : [ "TODAY", "BEFORE_TODAY" ]
              },
              "contractDateLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "dateFromLimitType" : {
                "type" : "string",
                "enum" : [ "CONTRACT_DATE", "CONTRACT_DATE_PLUS_ONE", "AFTER_CONTRACT_DATE" ]
              },
              "dateFromLimit" : {
                "type" : "integer",
                "format" : "int32"
              },
              "freeDateTo" : {
                "type" : "boolean",
                "default" : false
              },
              "minDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "maxDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "defaultDateTo" : {
                "$ref" : "#/definitions/ContractDateToLimit"
              },
              "periods" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffPeriod"
                }
              },
              "roundPayment" : {
                "type" : "boolean",
                "default" : false
              },
              "actionDescription" : {
                "type" : "string"
              },
              "discountDescription" : {
                "type" : "string"
              },
              "blankUsage" : {
                "type" : "string",
                "enum" : [ "NOT_USED", "USED", "EXTERNAL" ]
              },
              "blankAutoSelect" : {
                "type" : "boolean",
                "default" : false
              },
              "blankSearchUp" : {
                "type" : "boolean",
                "default" : false
              },
              "hasBeneficiary" : {
                "type" : "boolean",
                "default" : false
              },
              "proportionalUrgency" : {
                "type" : "boolean",
                "default" : false
              },
              "urgencyValues" : {
                "type" : "array",
                "items" : {
                  "type" : "number"
                }
              },
              "state" : {
                "type" : "string",
                "enum" : [ "DELETED", "DRAFT", "ACCEPTED" ]
              },
              "hasPartnerTariff" : {
                "type" : "boolean",
                "default" : false
              },
              "dateFromWithTime" : {
                "type" : "boolean",
                "default" : false
              },
              "insuranceObjectType" : {
                "type" : "string",
                "enum" : [ "NONE", "AUTO", "PERSON", "PROPERTY" ]
              },
              "maxObjectCount" : {
                "type" : "integer",
                "format" : "int32"
              },
              "risks" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffRisk"
                }
              },
              "customerSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "beneficiarySettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "miscSettings" : {
                "$ref" : "#/definitions/MiscFieldSettings"
              },
              "autoSettings" : {
                "$ref" : "#/definitions/AutoFieldSettings"
              },
              "propertySettings" : {
                "$ref" : "#/definitions/PropertyFieldSettings"
              },
              "personSettings" : {
                "$ref" : "#/definitions/CustomerFieldSettings"
              },
              "paymentOptions" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "calculationKinds" : {
                "type" : "array",
                "items" : {
                  "type" : "string"
                }
              },
              "commission" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "brokerDiscount" : {
                "type" : "number",
                "minimum" : 0.0,
                "maximum" : 1.0
              },
              "limits" : {
                "type" : "array",
                "items" : {
                  "$ref" : "#/definitions/TariffVclLimit"
                }
              },
              "contractType" : {
                "$ref" : "#/definitions/ContractType"
              }
            },
            "description" : "Базовый класс тарифов"
          },
          "TariffVclLimit" : {
            "type" : "object",
            "required" : [ "limit", "payment" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64"
              },
              "zone" : {
                "type" : "integer",
                "format" : "int32"
              },
              "autoCategory" : {
                "type" : "string",
                "enum" : [ "A1", "A2", "B1", "B2", "B3", "B4", "C1", "C2", "D1", "D2", "E", "F" ]
              },
              "limit" : {
                "type" : "number"
              },
              "payment" : {
                "type" : "number"
              }
            }
          },
          "TerritoryCountry" : {
            "type" : "object",
            "required" : [ "country" ],
            "properties" : {
              "country" : {
                "$ref" : "#/definitions/Country"
              },
              "currency" : {
                "type" : "string",
                "enum" : [ "UAH", "USD", "EUR", "RUB" ]
              }
            }
          },
          "Tourism" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "country", "coverageDays", "coverageTerritory", "insuranceAmount" ],
              "properties" : {
                "multivisa" : {
                  "type" : "boolean",
                  "description" : "Мультивиза",
                  "default" : false
                },
                "coverageDays" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "Количество дней покрытия"
                },
                "insuranceAmount" : {
                  "type" : "number",
                  "description" : "Страховая сумма"
                },
                "coverageTerritory" : {
                  "description" : "Территория покрытия",
                  "$ref" : "#/definitions/CoverageTerritory"
                },
                "country" : {
                  "description" : "Страна поездки",
                  "$ref" : "#/definitions/Country"
                }
              },
              "description" : "Договор туризма (ВЗР)"
            } ]
          },
          "User" : {
            "type" : "object",
            "required" : [ "dateFrom", "salePoint" ],
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор пользователя"
              },
              "lastName" : {
                "type" : "string",
                "description" : "Фамилия пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "firstName" : {
                "type" : "string",
                "description" : "Имя пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "middleName" : {
                "type" : "string",
                "description" : "Отчество пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "email" : {
                "type" : "string",
                "description" : "Электронная почта пользователя"
              },
              "code" : {
                "type" : "string",
                "description" : "ИНН пользователя",
                "minLength" : 10,
                "maxLength" : 10
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор пользователя во внешней системе системе (в системе компании)"
              },
              "salePoint" : {
                "description" : "точка продаж которой принадлежит пользователь",
                "$ref" : "#/definitions/SalePointSimple"
              },
              "password" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 40
              },
              "phone" : {
                "type" : "string",
                "minLength" : 12,
                "maxLength" : 13
              },
              "permissions" : {
                "$ref" : "#/definitions/UserPermissions"
              },
              "dateFrom" : {
                "type" : "string",
                "format" : "date-time"
              },
              "dateTo" : {
                "type" : "string",
                "format" : "date-time"
              },
              "settings" : {
                "$ref" : "#/definitions/UserSettings"
              }
            },
            "description" : "Пользователь системы"
          },
          "UserPermissions" : {
            "type" : "object",
            "required" : [ "canEditElectronicPolicy", "canEditPolicy", "canEditSalePoints", "canEditTariffs", "canEditUsers", "canEditWiki", "canSearchCustomers", "canViewPolicy", "companyAdmin", "superAdmin" ],
            "properties" : {
              "canEditPolicy" : {
                "type" : "string",
                "enum" : [ "NONE", "CREATE", "SIGN", "UNSIGN", "REQUEST", "EMIT" ]
              },
              "canEditElectronicPolicy" : {
                "type" : "string",
                "enum" : [ "NONE", "CREATE", "SIGN", "UNSIGN", "REQUEST", "EMIT" ]
              },
              "canViewPolicy" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canEditBlanks" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canSearchCustomers" : {
                "type" : "string",
                "enum" : [ "OWN", "SALE_POINT", "SALE_POINT_AND_CHILDS", "ALL_BY_COMPANY" ]
              },
              "canEditSalePoints" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditUsers" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditTariffs" : {
                "type" : "boolean",
                "default" : false
              },
              "canEditWiki" : {
                "type" : "boolean",
                "default" : false
              },
              "superAdmin" : {
                "type" : "boolean",
                "default" : false
              },
              "companyAdmin" : {
                "type" : "boolean",
                "default" : false
              }
            }
          },
          "UserSettings" : {
            "type" : "object",
            "properties" : {
              "printOffsetX" : {
                "type" : "integer",
                "format" : "int32"
              },
              "printOffsetY" : {
                "type" : "integer",
                "format" : "int32"
              }
            }
          },
          "UserSimple" : {
            "type" : "object",
            "properties" : {
              "id" : {
                "type" : "integer",
                "format" : "int64",
                "description" : "Идентификатор пользователя"
              },
              "lastName" : {
                "type" : "string",
                "description" : "Фамилия пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "firstName" : {
                "type" : "string",
                "description" : "Имя пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "middleName" : {
                "type" : "string",
                "description" : "Отчество пользователя",
                "minLength" : 0,
                "maxLength" : 50
              },
              "email" : {
                "type" : "string",
                "description" : "Электронная почта пользователя"
              },
              "code" : {
                "type" : "string",
                "description" : "ИНН пользователя",
                "minLength" : 10,
                "maxLength" : 10
              },
              "externalId" : {
                "type" : "string",
                "description" : "Идентификатор пользователя во внешней системе системе (в системе компании)"
              }
            },
            "description" : "Пользователь системы (упрощенный)"
          },
          "Vcl" : {
            "allOf" : [ {
              "$ref" : "#/definitions/Contract"
            }, {
              "type" : "object",
              "required" : [ "limit" ],
              "properties" : {
                "limit" : {
                  "type" : "number"
                }
              },
              "description" : "Полис \"ДГО\""
            } ]
          }
        }
      },
      "custom": {
        articles: {
          "default": {
            title: "Общие положения",
            body: {
              "description": {
                type: "ElementDescription",
                text: 
                  '<p><b>ВНИМАНИЕ! Для работы с сервисами EWA нужны cookies</b></p>' +
      
                  '<hr>' +
                  '<h3>СТРУКТУРА ДОКУМЕНТАЦИИ</h3>' +
                  '<ul>' +
                  '  <li><p>В секции <a>API</a> представлена информация о всех сервисах, доступных в EWA.' +
                  '    В разделе <a>API->АКТИВНЫЕ СЕРВИСЫ</a> описаны сервисы, которые должны быть(могут быть) реализованы' +
                  '    на стороне клиента, для обработки запросов EWA к внутренним системам клиента' +
                  '  </p></li>' +
                  '  <li><p>В секции <a>МОДЕЛИ ДАННЫХ</a> представлена информация о моделях, с которыми работают сервисы EWA</p></li>' +
                  '</ul>' +
                  
                  '<hr>' +
                  '<h3>ПОСРЕДНИКУ</h3>' +
                  '<ul>' +
                  '  <li><p>Перед обращением к любому сервису EWA необходимо пройти авторизацию. Смотреть пункт ' +
                  '    <a href="#api-/user/login">API->ПОЛЬЗОВАТЕЛИ->Вход в систему</a>' +
                  '  </p></li>' +
                  '  <li><p>Сервисы для работы со справочниками(например, справочник населенных пукнтов) представлены в разделе <a>API->СПРАВОЧНИКИ</a></p></li>' +
                  '  <li><p>Сервисы для расчета стоимости договора представлены в разделе <a>API->ТАРИФЫ</a>. Например, информация о сервисе для' +
                  '    расчета стоимости договора ОСАГО находится в пункте <a href="#api-/tariff/choose/policy">API->ТАРИФЫ->Калькулятор "ОСАГО"</a>' +
                  '  </p></li>' +
                  '  <li><p>Сервис для сохранения договора описан в пункте <a href="#api-/contract/save">API->ДОГОВОРЫ->Сохранение договора</a></p></li>' +
                  '</ul>' +
                  
                  '<hr>' +
                  '<h3>СТРАХОВЩИКУ</h3>' +
                  '<h4>Регистрация собственного адреса</h4>' +
                  '<p>Это необходимо для того, чтобы при входе в систему по собственному адресу на форме логина отображался персональный логотип компании</p>' +
                  '<p> Для регистрации компании company.ua(адрес взят для примера) необходимо в DNS-настройках домена company.ua добавить CNAME-запись' +
                  '  ewa.company.ua со ссылкой на хост web.ewa.ua'+
                  '</p>' +
                  '<h4>SSL-сертификат</h4>' +
                  '<p> Соединение с <a href="https://web.ewa.ua">web.ewa.ua</a> защищено и шифруется при помощи SSL-сертификата.' +
                  '  Для защиты собственного адреса компании необходим SSL-сертификат для адреса ewa.company.ua. Если у компании' +
                  '  уже есть SSL-сертификат с защитой субдоменов (Wildcard) *.company.ua, то дополнительный сертификат не нужен.' +
                  '  Если имеющийся сертификат не распространяется на адрес ewa.company.ua, то достаточно приобрести SSL-сертификат' +
                  '  непосредственно на адрес ewa.company.ua сроком от 1 года (к примеру, на <a href="https://ssl.com.ua">ssl.com.ua</a>)' +
                  '</p>'
              }
            }
          },
          "cURL": {
            title: "Отправка запроса через cURL",
            body: {
            }
          },
          "XMLHttpRequest": {
            title: "Отправка запроса через XMLHttpRequest",
            body: {
            }
          },
          "date-time_format": {
            title: "Формат дата-время, используемый в EWA",
            body: {
              "description": {
                type: "ElementDescription",
                text:  
                  '<p><b>В система EWA для представления даты-времени используется стандарт UTC. <u>Время по Гринвичу</u></b></p>' +
                  '<h2>Формат дата-время</h2>' +
                  '<pre>yyyy-mm-ddThh:mm:ss.SSS+zzzz</pre>' +
                  '<p>где</p>' +
                  '<ul>' +
                  '  <li><p>"yyyy" - год</p></li>' +
                  '  <li><p>"mm" - месяц</p></li>' +
                  '  <li><p>"dd" - день</p></li>' +
                  '  <li><p>"T" - разделитель даты и времени</p></li>' +
                  '  <li><p>"hh" - часы</p></li>' +
                  '  <li><p>"mm" - минуты</p></li>' +
                  '  <li><p>"ss" - секунды</p></li>' +
                  '  <li><p>"SSS" - милисекунды</p></li>' +
                  '  <li><p>"zzzz" - временная зона</p></li>' +
                  '</ul>' +
                  '<h2>Формат дата</h2>' +
                  '<pre>yyyy-mm-dd</pre>' +
                  '<p><b>ВНИМАНИЕ!!!</b> Рассмотрим на примере даты начала действия договора</p>' +
                  '<p> Данное поле использует тип дата-время. Допустим договор начинает действовать c <u>02.02.2016</u> и заключен в <u>часовом поясе +2</u>.' +
                  '  В структуре модели данных EWA время будет отображено как <u>01.01.2016T22:00:00.000+0000</u>' +
                  '</p>'
              }
            }
          },
          "api-active_services-general": {
            title: "Общие положения",
            body: {
              "description": {
                type: "ElementDescription",
                text:
                  '<p>Активные веб-сервисы срабатывают при неких событиях в EWA (например, при сохранении договора) либо по расписанию. При наступлении события формируется REST-запрос и отправляется на сервер клиенту.<p>' +
                  '<h3>Процедура настройки обмена данными между системой EWA и внешней системой клиента</h3>' +
                  '<ul>' +
                  '  <li><p>Клиент со своей стороны реализует только необходимые ему сервисы</p></li>' +
                  '  <li><p>Все активные WEB-сервисы независимы. Каждый сервис настраивается отдельно</p></li>' +
                  '  <li><p>Для каждого WEB-сервиса клиент выбирает необходимый ему метод REST-запроса (GET, POST и т.д.)</p></li>' +
                  '  <li><p>При необходимости в запрос можно добавить любое количество параметров-констант. Параметры могут быть установлены как в заголовок так и тело</p></li>' +
                  '  <li><p>В зависимости от типа события, которое произошло в EWA, в запрос могут быть добавлены служебные параметры. Список служебных параметров описан для каждого сервиса отдельно. Клиент сам решает, какие служебные параметры нужно добавить.</b></p></li>' +
                  '</ul>' +
                  '<h3>ВАЖНО!!!</h3>' +
                  '<ul>' +
                  '  <li><p>Формат ответа должен полностью отвечать требованиям EWA. Иначе запрос будет считаться невыполненным</p></li>' +
                  '  <li><p>Тип и формат (например, для даты) служебных параметров изменить нельзя</p></li>' +
                  '  <li><p>В описании служебных параметров указаны имена по умолчанию. По желанию клиента, имена могут быть изменены</p></li>' +
                  '</ul>'
              }
            }
          },
          "api-active_services-auth": {
            title: "Авторизация на сервере клиента"
          },
          "api-active_services-person": {
            title: "Получение списка контрагентов по ИНН",
            body: {
              "description": {
                type: "ElementDescription",
                order: 0,
                text: 'Сервис используется для получения списка контрагентов, идентификационный номер которых начинается с [query].'
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "isLegal": {
                    description: "Признак. Юридическое лицо",
                    order: 0,
                    paramType: {
                      name: "boolean"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 1,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  },
                  "documentTypes": {
                    description: "Список допустимых типов для документа, удостоверяюещего личность. Разделитель списка - запятая",
                    order: 2,
                    paramType: {
                      name: "string",
                      allowedValues: {
                        "PASSPORT": {},
                        "DRIVING_LICENSE": {},
                        "RESIDENCE_PERMIT": {},
                        "VETERAN_CERTIFICATE": {},
                        "REGISTRATION_CARD": {},
                        "EXTERNAL_PASSPORT": {},
                        "FOREIGN_PASSPORT": {},
                        "PENSION_CERTIFICATE": {},
                        "DISABILITY_CERTIFICATE": {},
                        "CHERNOBYL_CERTIFICATE": {}
                      }
                    }
                  },
                  "query": {
                    description: "Идентификационный номер контрагента",
                    order: 3,
                    paramType: {
                      name: "string"
                    }
                  },
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных контрагентов",
                    paramType: {
                      name: "array of ",
                      ref: "model-Person",
                      refName: "Person"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-auto-vin": {
            title: "Поиск транспортного средства по VIN-коду",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка транспортных средств, VIN-код которых начинается с [vin].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "vin": {
                    description: "Начальные символы VIN-кода",
                    paramType: {
                      name: "string"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных транспортных средств",
                    paramType: {
                      name: "array of ",
                      ref: "model-InsuranceObjectAuto",
                      refName: "InsuranceObjectAuto"
                    }
                  }
                }
              }
            },
          },
          "api-active_services-auto-state_number": {
            title: "Поиск транспортного средства по государственному номеру",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка транспортных средств, государственный номер которых начинается с [stateNumber].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "stateNumber": {
                    description: "Начальные символы государственного номера",
                    paramType: {
                      name: "string"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных транспортных средств",
                    paramType: {
                      name: "array of ",
                      ref: "model-InsuranceObjectAuto",
                      refName: "InsuranceObjectAuto"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-blank": {
            title: "Получение списка бланков по номеру и типу на дату",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка бланков, номера которых начинаются с [query].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "query": {
                    description: "Первые несколько символов номера",
                    order: 0,
                    paramType: {
                      name: "string"
                    }
                  },
                  "blankType": {
                    description: "Тип бланка",
                    order: 1,
                    paramType: {
                      name: "string",
                    }
                  },
                  "blankSearchUp": {
                    description: "Признак. Искать бланк начиная с текущей точки продаж и вверх по дереву агентской сети",
                    order: 2,
                    paramType: {
                      name: "boolean"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 3,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных номеров",
                    paramType: {
                      name: "array of ",
                      refName: "string"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-agent": {
            title: "Получение списка агентов по коду на дату",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис используется для получения списка агентов, номер которых начинается с [query].',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "query": {
                    description: "Первые несколько символов кода",
                    order: 0,
                    paramType: {
                      name: "string"
                    }
                  },
                  "date": {
                    description: "Дата актуальности. Данные должны быть актуальны на эту дату",
                    order: 1,
                    paramType: {
                      name: "string",
                      format: "date-time"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Список найденных кодов агентов",
                    paramType: {
                      name: "array of ",
                      refName: "string"
                    }
                  }
                }
              }
            }
          },
          "api-active_services-export": {
            title: "Экспорт данных о реализованном договоре",
            body: {
              "description": {
                type: "ElementDescription",
                text: 'Сервис экспортирует данные о реализованном договоре при любом его изменении. <br><b>ВНИМАНИЕ! Договор не будет выгружен, если закрыт период вызгрузки в настройках компании.</b>',
                order: 0
              },
              "Request:": {
                type: "ElementParamsGroup",
                title: "Request:",
                params: {
                  "contract": {
                    description: "Данные договора",
                    paramType: {
                      ref: "model-Contract",
                      refName: "Contract"
                    }
                  }
                }
              },
              "Response 200:": {
                type: "ElementParamsGroup",
                title: "Response 200:",
                hasField: false,
                params: {
                  "body": {
                    description: "Информация о состоянии выгрузки",
                    paramType: {
                      ref: "model-ExportResult",
                      refName: "ExportResult"
                    }
                  }
                }
              }
            }
          },
        },
        navigation: {
          sections: {
            "general": {
              order: 0,
              title: "Общая информация",
              tags: {
                "general": {
                  articles: {
                    "default": {
                      title: "Общие положения",
                      order: 0
                    },
                    "cURL": {
                      title: "Отправка запроса через cURL",
                      order: 1
                    },
                    "XMLHttpRequest": {
                      title: "Отправка запроса через XMLHttpRequest",
                      order: 1
                    },
                    "date-time_format": {
                      title: "Формат дата-время",
                      order: 2
                    }
                  }
                }
              }
            },
            "api": {
              tags: {
                "api-active_services": {
                  order: 0,
                  title: "Активные сервисы",
                  articles: {
                    "api-active_services-general": {
                      order: 0,
                      title: "Общие положения"
                    },
                    "api-active_services-auth": {
                      order: 1,
                      title: "Авторизация на сервере клиента"
                    },
                    "api-active_services-person": {
                      order: 2,
                      title: "Поиск контрагента по ИНН"
                    },
                    "api-active_services-blank": {
                      order: 3,
                      title: "Поиск бланка, стикера"
                    },
                    "api-active_services-agent": {
                      order: 4,
                      title: "Поиск агента"
                    },
                    "api-active_services-auto-vin": {
                      order: 5,
                      title: "Поиск ТС по VIN"
                    },
                    "api-active_services-auto-state_number": {
                      order: 6,
                      title: "Поиск ТС по гос. номеру"
                    },
                    "api-active_services-export": {
                      order: 7,
                      title: "Экспорт договора"
                    }
                  }
                }
              }
            }
          }
        }
      },
      "templates": [
        {
          objectType: "ElementAllowedValue",
          list: [
            {
              condition: {id: "PASSPORT"},
              transform: {description: "паспорт"}
            }, {
              condition: {id: "DRIVING_LICENSE"},
              transform: {description: "водительское удостоверение"}
            }, {
              condition: {id: "RESIDENCE_PERMIT"},
              transform: {description: "вид на жительство"}
            }, {
              condition: {id: "VETERAN_CERTIFICATE"},
              transform: {description: "удостоверение участника войны"}
            }, {
              condition: {id: "REGISTRATION_CARD"},
              transform: {description: "регистрационный талон"}
            }, {
              condition: {id: "EXTERNAL_PASSPORT"},
              transform: {description: "загран паспорт"}
            }, {
              condition: {id: "FOREIGN_PASSPORT"},
              transform: {description: "иностранный паспорт"}
            }, {
              condition: {id: "PENSION_CERTIFICATE"},
              transform: {description: "пенсионное удостоверение"}
            }, {
              condition: {id: "DISABILITY_CERTIFICATE"},
              transform: {description: "удостоверение об инвалидности"}
            }, {
              condition: {id: "CHERNOBYL_CERTIFICATE"},
              transform: {description: "Чернобыльское удостоверение"}
            }
          ]
        }
      ],
    },
  }
});
